<?php 
function AssociativeArrayToStr($array, $key_to_concat, $sep) {
	$i = 0;
	$string = "";
	foreach ($array as $key => $values) {
		foreach ($values as $k => $v) {
			if($k == $key_to_concat) {
				if($i == 0)
					$string = $v;
				else
					$string .= $sep .$v;
				$i++;
			}
		}
	}
	return $string;
}

function generateRandomString($length = 10) 
{
    $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function objectToArray ($object) {
    if(!is_object($object) && !is_array($object))
        return $object;

    return array_map('objectToArray', (array) $object);
}

function get_random_number($digits_needed){
    $count=0;
    while ( $count < $digits_needed ) {
        $random_digit = mt_rand(0, 9);
        $random_number .= $random_digit;
        $count++;
    }
}

function tj_array_column($assoc_array, $column) {
	$i = 0;
	$return_array = array();
	if (is_array($assoc_array)) {
		foreach ($assoc_array as $array) {
			$return_array[$i] = $array[$column];
			$i++;
		}
	}
	
	return $return_array;
}


function myUrlDecode($string) {
	$entities =     array('!26','!26', '!2A', '!27', '!28', '!29', '!3B', '!3A', '!40', '!3D', '!2B', '!24', '!2C', '!2F', '!3F', '!25', '!23', '!5B', '!5D', '!6A', '!6B');
	$replacements = array("&amp;",'&', '*',   "'",   "(",   ")",   ";",   ":",   "@",    "=",   "+",   "$",   ",",   "/",   "?",   "%",   "#",   "[",   "]",   ' '  , '.' );
	return str_replace($entities, $replacements, $string);
}

function myUrlEncode($string) {
	$replacements =  array('!26', '!26', '!2A', '!27', '!28', '!29', '!3B', '!3A', '!40', '!3D', '!2B', '!24', '!2C', '!2F', '!3F', '!25', '!23', '!5B', '!5D', '!6A', '!6B');
	$entities     =  array('&amp; ','&',   '*',   "'",   "(",   ")",   ";",   ":",   "@",    "=",   "+",   "$",   ",",   "/",   "?",   "%",   "#",   "[",   "]",   ' '  , '.' );
	return str_replace($entities, $replacements, $string);
}

function print_array($array){
	echo "<pre>";
	print_r($array);
	exit;
}

function print_view($array){
    echo "<pre>";
    print_r($array);
}

function getResultArray($Q){
	$data=array();
	if($Q->num_rows()>0){
		foreach($Q->result_array() as $row){
			$data[]=$row;
		}
	}
	return $data;
}

function flatten(array $array) {
    $return = array();
    array_walk_recursive($array, function($a) use (&$return) { $return[] = $a; });
    return $return;
}

function SimpleArray($search_array){
	$data=array();
	foreach($search_array as $row){
		foreach($row as $k => $v)
		$data[]=$v;
	}
	return $data;
}

function getResultArraySimpleArray($Q){
	$data=array();
	if($Q->num_rows()>0){
		foreach($Q->result_array() as $row){
			foreach($row as $k => $v) $data[]=$v;
		}
	}
	return $data;
}

function upto2Decimal($price=''){
   return number_format((float)$price, 2, '.', '');
}

function getCurrentDate() {
	$now = date("Y-m-d h:i:s");
	return $now;
}

function getMonthsDD() {
	$month_array=array(
			'0'=>"Month",
			'1'=>"January",
			'2'=>"February",
			'3'=>"March",
			'4'=>"April",
			'5'=>"May",
			'6'=>"June",
			'7'=>"July",
			'8'=>"August",
			'9'=>"September",
			'10'=>"October",
			'11'=>"November",
			'12'=>"December");
	return $month_array;
}

function getMonthsSmall(){
	$month_array=array(
			'6'=>"Jun",
			'7'=>"Jul",
			'8'=>"Aug",
			'9'=>"Sep",
			'10'=>"Oct",
			'11'=>"Nov",
			'12'=>"Dec",
			'1'=>"Jan",
			'2'=>"Feb",
			'3'=>"Mar",
			'4'=>"Apr",
			'5'=>"May",);
	return $month_array;
}


/*

|--------------------------------------------------------------------------

| Following Functions Created By Paperplane @rv!nd

|--------------------------------------------------------------------------

*/


function findKey($array, $keySearch) {
	foreach ($array as $key => $item) {
		if ($key == $keySearch) {
			return 1;
		} else {
			if (isset($array[$key]))
				findKey($array[$key], $keySearch);
		}
	}
	return 0;
}

function filter_unique_array($arrs, $id) {
    foreach($arrs as $k => $v) 
    {
        foreach($arrs as $key => $value) 
        {
            if($k != $key && $v[$id] == $value[$id])
            {
                 unset($arrs[$k]);
            }
        }
    }
    return $arrs;
}


/*END OF @rv!nd*/

function copyImage($imageSrc, $imageDest) {
	$src = urldecode($imageSrc);
	$len1 = strlen($src);
	$len2 = strlen(base_url());
	$fpath = substr($src, $len2 - 1, $len1);
	$pathParts = explode('/', $src);
	$oldFileName = $pathParts[count($pathParts) - 1];
	$ext = explode('.',$oldFileName);
	$newFileName = generateRandomCode().'.'.$ext[1];
	$srcCopy = $_SERVER['DOCUMENT_ROOT'].$fpath;
	$destCopy = $_SERVER['DOCUMENT_ROOT'].$imageDest.$newFileName;
	
	$copy = copy($srcCopy, $destCopy);
	unlink($srcCopy);
	if($copy == false) {
		return false;
	}
	return $newFileName;
}

function generateRandomCode() {
	$d=date ("d");
	$m=date ("m");
	$y=date ("Y");
	$t=time();
	$dmt=$d+$m+$y+$t;
	$ran= rand(0,10000000);
	$dmtran= $dmt+$ran;
	$un=  uniqid();
	$dmtun = $dmt.$un;
	$mdun = md5($dmtran.$un);
	$sort=substr($mdun, 16); // if you want sort length code.
	return $mdun;
}

/**
 *
 * Function to make URLs into links
 *
 * @param string The url string
 *
 * @return string
 *
 **/
function makeLink($string, $label){

	/*** make sure there is an http:// on all URLs ***/
	$string = preg_replace("/([^\w\/])(www\.[a-z0-9\-]+\.[a-z0-9\-]+)/i", "$1http://$2",$string);
	/*** make all URLs links ***/
	$string = preg_replace("/([\w]+:\/\/[\w-?&;#~=\.\/\@]+[\w\/])/i","<a target=\"_blank\" href=\"$1\">.$label.</a>",$string);
	/*** make all emails hot links ***/
	$string = preg_replace("/([\w-?&;#~=\.\/]+\@(\[?)[a-zA-Z0-9\-\.]+\.([a-zA-Z]{2,3}|[0-9]{1,3})(\]?))/i","<a href=\"mailto:$1\">$1</a>",$string);

	return $string;
}

function highlightkeyword($str, $search) {
    $highlightcolor = "#daa732";
    $occurrences = substr_count(strtolower($str), strtolower($search));
    $newstring = $str;
    $match = array();
 
    for ($i=0;$i<$occurrences;$i++) {
        $match[$i] = stripos($str, $search, $i);
        $match[$i] = substr($str, $match[$i], strlen($search));
        $newstring = str_replace($match[$i], '[#]'.$match[$i].'[@]', strip_tags($newstring));
    }
 
    $newstring = str_replace('[#]', '<span style="color: '.$highlightcolor.';">', $newstring);
    $newstring = str_replace('[@]', '</span>', $newstring);
    return $newstring;
 
}

function whatever($array, $key, $val) {
    foreach ($array as $item)
        if (isset($item[$key]) && $item[$key] == $val)
            return true;
    return false;
}

function whatever_return($array, $key, $val) {
	foreach ($array as $item)
        if (isset($item[$key]) && $item[$key] == $val)
            return $item;
    return false;
}

function find_key_in_array($array, $key) {
	$result = array();
	foreach ($array as $item){
        if (isset($item[$key])){
            $result[]=$item;
        }
	}
    return $result;
}

function custom_sort($array,$order){ //length of order and array to be same
	usort($array, function ($a, $b) use ($order) {
        $pos_a = array_search($a, $order);
        $pos_b = array_search($b, $order);
        return $pos_a - $pos_b;
    });
    return $array;
}

function sortArrayByArray($array,$orderArray) {
    $ordered = array();
    foreach($orderArray as $key => $value) {
        if(array_key_exists($key,$array)) {
                $ordered[$key] = $array[$key];
                unset($array[$key]);
        }
    }
    return $ordered+$array;
}

function convert_number_to_words($number) {
    
    $hyphen      = '-';
    $conjunction = ' and ';
    $separator   = ', ';
    $negative    = 'negative ';
    $decimal     = ' point ';
    $dictionary  = array(
        0                   => 'zero',
        1                   => 'one',
        2                   => 'two',
        3                   => 'three',
        4                   => 'four',
        5                   => 'five',
        6                   => 'six',
        7                   => 'seven',
        8                   => 'eight',
        9                   => 'nine',
        10                  => 'ten',
        11                  => 'eleven',
        12                  => 'twelve',
        13                  => 'thirteen',
        14                  => 'fourteen',
        15                  => 'fifteen',
        16                  => 'sixteen',
        17                  => 'seventeen',
        18                  => 'eighteen',
        19                  => 'nineteen',
        20                  => 'twenty',
        30                  => 'thirty',
        40                  => 'fourty',
        50                  => 'fifty',
        60                  => 'sixty',
        70                  => 'seventy',
        80                  => 'eighty',
        90                  => 'ninety',
        100                 => 'hundred',
        1000                => 'thousand',
        1000000             => 'million',
        1000000000          => 'billion',
        1000000000000       => 'trillion',
        1000000000000000    => 'quadrillion',
        1000000000000000000 => 'quintillion'
    );
    
    if (!is_numeric($number)) {
        return false;
    }
    
    if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
        // overflow
        trigger_error(
            'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
            E_USER_WARNING
        );
        return false;
    }

    if ($number < 0) {
        return $negative . convert_number_to_words(abs($number));
    }
    
    $string = $fraction = null;
    
    if (strpos($number, '.') !== false) {
        list($number, $fraction) = explode('.', $number);
    }
    
    switch (true) {
        case $number < 21:
            $string = $dictionary[$number];
            break;
        case $number < 100:
            $tens   = ((int) ($number / 10)) * 10;
            $units  = $number % 10;
            $string = $dictionary[$tens];
            if ($units) {
                $string .= $hyphen . $dictionary[$units];
            }
            break;
        case $number < 1000:
            $hundreds  = $number / 100;
            $remainder = $number % 100;
            $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
            if ($remainder) {
                $string .= $conjunction . convert_number_to_words($remainder);
            }
            break;
        default:
            $baseUnit = pow(1000, floor(log($number, 1000)));
            $numBaseUnits = (int) ($number / $baseUnit);
            $remainder = $number % $baseUnit;
            $string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
            if ($remainder) {
                $string .= $remainder < 100 ? $conjunction : $separator;
                $string .= convert_number_to_words($remainder);
            }
            break;
    }
    
    if (null !== $fraction && is_numeric($fraction)) {
        $string .= $decimal;
        $words = array();
        foreach (str_split((string) $fraction) as $number) {
            $words[] = $dictionary[$number];
        }
        $string .= implode(' ', $words);
    }
    
    return $string;
}

function FindImageURL($image_url='',$imagename=''){
    //$imageurl = $image_url.$imagename;
    //$url=getimagesize($imageurl);
    $imageurl = '';
    list($width, $height, $type, $attr) = getimagesize($_SERVER['DOCUMENT_ROOT'] . '/contourtek/crawled_image/'.$imagename);  
    //echo 'height:'.$width; exit;
    //print_array($_SERVER['DOCUMENT_ROOT']);exit;

    if(isset($width) && $width!=''){
        $imageurl = $image_url.$imagename;//$_SERVER['DOCUMENT_ROOT'] . '/contourtek/crawled_image/'.$imagename;
    }else{
        $imageurl = base_url('assets/images').'/default_img.gif';
    }
    
    return $imageurl;
}



if(! function_exists("null2unknown") ){
    function null2unknown($data) {
        if ($data == "") {
            return "No Value Returned";
        } else {
            return $data;
        }
    } 
}
if(! function_exists("getResponseDescription") ){
    function getResponseDescription($responseCode) {

        switch ($responseCode) {
            case "0" : $result = "Transaction Successful"; break;
            case "?" : $result = "Transaction status is unknown"; break;
            case "1" : $result = "Unknown Error"; break;
            case "2" : $result = "Bank Declined Transaction"; break;
            case "3" : $result = "No Reply from Bank"; break;
            case "4" : $result = "Expired Card"; break;
            case "5" : $result = "Insufficient funds"; break;
            case "6" : $result = "Error Communicating with Bank"; break;
            case "7" : $result = "Payment Server System Error"; break;
            case "8" : $result = "Transaction Type Not Supported"; break;
            case "9" : $result = "Bank declined transaction (Do not contact Bank)"; break;
            case "A" : $result = "Transaction Aborted"; break;
            case "C" : $result = "Transaction Cancelled"; break;
            case "D" : $result = "Deferred transaction has been received and is awaiting processing"; break;
            case "F" : $result = "3D Secure Authentication failed"; break;
            case "I" : $result = "Card Security Code verification failed"; break;
            case "L" : $result = "Shopping Transaction Locked (Please try the transaction again later)"; break;
            case "N" : $result = "Cardholder is not enrolled in Authentication scheme"; break;
            case "P" : $result = "Transaction has been received by the Payment Adaptor and is being processed"; break;
            case "R" : $result = "Transaction was not processed - Reached limit of retry attempts allowed"; break;
            case "S" : $result = "Duplicate SessionID (OrderInfo)"; break;
            case "T" : $result = "Address Verification Failed"; break;
            case "U" : $result = "Card Security Code Failed"; break;
            case "V" : $result = "Address Verification and Card Security Code Failed"; break;
            default  : $result = "Unable to be determined"; 
        }
        return $result;
    }
}
if(! function_exists("getStatusDescription") ){
    function getStatusDescription($statusResponse) {
        if ($statusResponse == "" || $statusResponse == "No Value Returned") {
            $result = "3DS not supported or there was no 3DS data provided";
        } else {
            switch ($statusResponse) {
                case "Y"  : $result = "The cardholder was successfully authenticated."; break;
                case "E"  : $result = "The cardholder is not enrolled."; break;
                case "N"  : $result = "The cardholder was not verified."; break;
                case "U"  : $result = "The cardholder's Issuer was unable to authenticate due to some system error at the Issuer."; break;
                case "F"  : $result = "There was an error in the format of the request from the merchant."; break;
                case "A"  : $result = "Authentication of your Merchant ID and Password to the ACS Directory Failed."; break;
                case "D"  : $result = "Error communicating with the Directory Server."; break;
                case "C"  : $result = "The card type is not supported for authentication."; break;
                case "S"  : $result = "The signature on the response received from the Issuer could not be validated."; break;
                case "P"  : $result = "Error parsing input from Issuer."; break;
                case "I"  : $result = "Internal Payment Server system error."; break;
                default   : $result = "Unable to be determined"; break;
            }
        }
        return $result;
    }
}