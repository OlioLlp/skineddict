
<div class="modal fade" id="overlay">
  <div class="modal-dialog modalhome">
    	<button type="button" class="btn btn-modalclose" data-dismiss="modal">Close</button>
      <!-- Modal content-->
      <div class="modal-content usermodal">
        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
        <div class="modal-body text-center">
          <h1 class="title">NEWSLETTER</h1>
          <p>Subscribe to the TheFace mailing list to receive updates on new arrivals, special offers and other discount information.</p>
          <input type="email" name="email" placeholder="Enter your Email Id">
          <div class="action">
          	<button class="btn btn-action">Subscribe</button>
          </div>
        </div>
      </div>
      
    </div>
</div>
<script type="text/javascript">
 $('#overlay').modal('show');
</script>
</header>
<section class="regular slider bannerimg" id="mainbanner">
	<!-- <div class="bannerimg"> -->
	<?php foreach($mainslider as $key=>$value){ ?>
		<div>
		    <img src="<?php echo base_url();?><?php echo $value['slider_image']; ?>">
	   		<div class="container">
				<div class="bannercontent">
					<h3 class="banner-title1"><?php echo $value['title1']; ?></h3>
					<h2 class="banner-title2"><?php echo $value['title2']; ?></h2>
					<h2 class="banner-title3"><?php echo $value['title3']; ?></h2>
					<a href="<?php echo $value['link']; ?>"><h4 class="bannerlink"><?php echo $value['link_name']; ?></h4></a>
				</div>
			</div>
	    </div>
	<?php } ?>
	    
	<!-- </div> -->
</section>
<div id="mainbannercontrols" class="hidden-xs hidden-sm">
		<div id="mainprev">Prev</div>
		<div id="mainnext">Next</div>
	</div>
<div class="container" style="margin: 10px auto 30px;">
	<div class="row">
		<div class="col-md-2 col-sm-12 col-xs-12">
			<div class="group-feature">
				<span>our</span>
				<h2>featured</h2>
				<span>products</span>
			</div>
			<div style="margin-top: 20%;" class="hidden-xs hidden-sm">
				<button id="prev2"><i class="fa fa-angle-left"></i></button>
			 	<button id="next2"><i class="fa fa-angle-right"></i></button>
			</div>
		</div>
		<div class="col-md-10 col-sm-12 col-xs-12">
			<section class="multiple-items slider" id="multislide">
			
			<?php foreach($featured as $key=>$value){ ?>
				
				<div class="slide2div">
						<div class="image-container">
						  <a href="<?php echo base_url(); ?>details/<?php echo $value['product_id']; ?>"><img src="<?php echo base_url().$value['images'];?>" alt="Avatar" class="image" style="width:100%"></a>
						  <div class="middle">
						    <div class="text">
						    	<p>
						 			<span><a  onclick='addtocart(<?php echo $value['product_id'];?>)'><i class="fa fa-plus"></i> ADD TO CART</a></span>
						 			<span><a href=""><i class="fa fa-eye"></i></a></span>
						 			<span><a href=""><i class="fa fa-heart"></i></a></span>
						 			<span><a href=""><i class="fa fa-exchange"></i></a></span>
						 		</p>
						    </div>
						  </div>
						</div>
					    <a href="<?php echo base_url(); ?>details/<?php echo $value['product_id']; ?>" class="link"><h4><?php echo $value['product_name'];?></h4></a>
						<?php if($value['offer_price'] != ""){ ?>
						
					    <p><span class="price1"><i class="fa fa-rupee"></i><?php echo $value['offer_price'];?></span>&nbsp;
						
						<?php } ?>
						
					    	<span class="<?php echo($value['offer_price'] != "" ? 'price2' : 'price1'); ?>"><i class="fa fa-rupee"></i><?php echo $value['price'];?></span></p>
				    </div>
				
			<?php	
				
			} ?>
			
			
			</section>
		</div>
	</div>
</div>

<input type="hidden" name="quantity" id="quantity" value="1">

<section class="page-main">
	<div id="home-bg">
		<div class="container">
			<div class="row">
				<section class="regular1 slider countdown-slider">
				
				<?php foreach($hotdeals as $key=>$value){
				?>
					<div>
				      <!-- <div class="row"> -->
				      	<div class="col-md-6 col-sm-12 col-xs-12">
				      		<img src="<?php echo base_url().$value['deal_image'];?>"
				      		class="img-responsive">
				      	</div>
				      	<div class="col-md-6 col-sm-12 col-xs-12">
				      		<div class="product-item-name text-center">
				      			<h2 class="group-title1"><span>HOT</span><span style="font-weight: bold;color: #222222;padding-left: 20px;">DEAL</span></h2>
				      			<div class="short-description">
				      				<p><?php echo $value['deal_description'];?></p>
				      			</div>
				      			<a href="<?php echo $value['link'];?>" style="text-decoration: underline; font-weight: 400;"><h4 class="linkcounter">Shop Collection Now!</h4></a>
				      		</div>
				      	</div>
				      <!-- </div> -->
				    </div>
					
					
				<?php 
				} ?>

				</section>
			</div>
		</div>
	</div>
</section>


<div class="container">
	<div class="row sectionspace">
		<div class="col-md-2">
			<div class="group-feature">
				<span>our</span>
				<h2>BEST SELLER</h2>
				<span>products</span>
			</div>
			<div style="margin-top: 20%;" class="hidden-xs hidden-sm">
				<button id="prev3"><i class="fa fa-angle-left"></i></button>
			 	<button id="next3"><i class="fa fa-angle-right"></i></button>
			</div>
		</div>
		<div class="col-md-10">
			<section class="multiple-items1 slider" id="multislide1">
			
				<?php foreach($best_seller as $key=>$value){ ?>
					
					<div class="slide2div">
							<div class="image-container">
							  <a href=""><img src="<?php echo base_url().$value['images'];?>" alt="Avatar" class="image" style="width:100%"></a>
							  <div class="middle">
								<div class="text">
									<p>
										<span><a  onclick='addtocart(<?php echo $value['product_id'];?>)'><i class="fa fa-plus"></i> ADD TO CART</a></span>
										<span><a href=""><i class="fa fa-eye"></i></a></span>
										<span><a href=""><i class="fa fa-heart"></i></a></span>
										<span><a href=""><i class="fa fa-exchange"></i></a></span>
									</p>
								</div>
							  </div>
							</div>
							<a href="<?php echo base_url(); ?>details/<?php echo $value['product_id']; ?>" class="link"><h4><?php echo $value['product_name'];?></h4></a>
							<?php if($value['offer_price'] != ""){ ?>
							
							<p><span class="price1"><i class="fa fa-rupee"></i><?php echo $value['offer_price'];?></span>&nbsp;
							
							<?php } ?>
							
								<span class="<?php echo($value['offer_price'] != "" ? 'price2' : 'price1'); ?>"><i class="fa fa-rupee"></i><?php echo $value['price'];?></span></p>
						</div>
					
				<?php	
					
				} ?>
			
			
				    
			</section>
		</div>
	</div>
</div>

<section id="cleasing" class="text-center">
	<div class="main-des">
		<img src="<?php echo base_url();?>assets/images/img_bannermid.jpg" class="img-responsive">
		<div class="container">
			<div class="des">
				<h3>A Single Step Skin Perfector!</h3>
				<h1><span>MAKEUP MELT</span><span>CLEANSING BALM</span></h1>
				<a href="" style="text-decoration: underline; font-weight: 400;"><h4 class="linkcounter animated zoomIn">Shop Collection Now!</h4></a>
			</div>
		</div>
	</div>
</section>

<div class="container">
	<div class="row sectionspace">
		<div class="col-md-2">
			<div class="group-feature">
				<span>our</span>
				<h2>2018 TREND</h2>
				<span>products</span>
			</div>
			<div style="margin-top: 20%;" class="hidden-xs hidden-sm">
				<button id="prev4"><i class="fa fa-angle-left"></i></button>
			 	<button id="next4"><i class="fa fa-angle-right"></i></button>
			</div>
		</div>
		<div class="col-md-10">
			<section class="multiple-items2 slider" id="multislide2">
			
			<?php foreach($trending as $key=>$value){ ?>
				
				<div class="slide2div">
						<div class="image-container">
						  <a href=""><img src="<?php echo base_url().$value['images'];?>" alt="Avatar" class="image" style="width:100%"></a>
						  <div class="middle">
						    <div class="text">
						    	<p>
						 			<span><a onclick='addtocart(<?php echo $value['product_id'];?>)'><i class="fa fa-plus"></i> ADD TO CART</a></span>
						 			<span><a href=""><i class="fa fa-eye"></i></a></span>
						 			<span><a href=""><i class="fa fa-heart"></i></a></span>
						 			<span><a href=""><i class="fa fa-exchange"></i></a></span>
						 		</p>
						    </div>
						  </div>
						</div>
					    <a href="<?php echo base_url(); ?>details/<?php echo $value['product_id']; ?>" class="link"><h4><?php echo $value['product_name'];?></h4></a>
						<p>
						<?php if($value['offer_price'] != ""){ ?>
						
					    <span class="price1"><i class="fa fa-rupee"></i><?php echo $value['offer_price'];?></span>&nbsp;
						
						<?php } ?>
						
					    	<span class="<?php echo($value['offer_price'] != "" ? 'price2' : 'price1'); ?>"><i class="fa fa-rupee"></i><?php echo $value['price'];?></span></p>
				    </div>
				
			<?php	
				
			} ?>
			
			
			</section>
		</div>
	</div>
</div>

<section id="bgsubscribe">
	<div class="container">
		<div class="row text-center">
			<div class="block-newsletter">
				<h3>
					<span>GET </span><span class="discount">10% </span><span> DISCOUNT</span>
				</h3>
				<p>Subcribe to the Skineddict Shoppe mailing list to receive update on mnew arrivals,<br>special offers and other discount information.</p>
				<div class="control">
					<input type="email" name="email" placeholder="yourmail@gmail.com">
					<button class="btn btn-custom actions">SUBSCRIBE</button>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="blog-slider">
	<div class="container">
		<div class="row">
			<div class="blog-title1 pull-left">
				<h2>FROM THE BLOG</h2>
			</div>
			<div class="pull-right">
				<button id="prevblog"><i class="fa fa-angle-left"></i></button>
			 	<button id="nextblog"><i class="fa fa-angle-right"></i></button>
			</div>
		</div>
		<div class="row">
			<section class="blogslider slider" id="blogslider1">
				<div class="slide2div">
					<div class="blog-container">
						<div class="blogimg">
							<a href=""><img src="<?php echo base_url();?>assets/images/blog1_1.png" alt="Avatar" class="image" style="width:100%"></a>
						</div>  
				    </div>
				    <div class="blog-content">
				    	<h3>Cool boy with tattoo</h3>
				    	<h5 class="time-conment"><i class="fa fa-calendar"></i>&nbsp; 20 - Jun</h5>
				    	<p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat</p>
				    	<a href="">Read More</a>
			    	</div>
			    </div>
			    <div class="slide2div">
			    	<div class="blog-container">
						<div class="blogimg">
							<a href=""><img src="<?php echo base_url();?>assets/images/blog2_1.png" alt="Avatar" class="image" style="width:100%"></a>
						</div>  
				    </div>
			      	<div class="blog-content">
				    	<h3>Hello World</h3>
				    	<h5 class="time-conment"><i class="fa fa-calendar"></i>&nbsp; 20 - Jun</h5>
				    	<p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat</p>
				    	<a href="">Read More</a>
				    </div>
			    </div>
			    <div class="slide2div">
			    	<div class="blog-container">
						<div class="blogimg">
							<a href=""><img src="<?php echo base_url();?>assets/images/blog3.png" alt="Avatar" class="image" style="width:100%"></a>
						</div>  
				    </div>
			      	<div class="blog-content">
				    	<h3>How to ROCK A BOY</h3>
				    	<h5 class="time-conment"><i class="fa fa-calendar"></i>&nbsp; 20 - Jun</h5>
				    	<p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat</p>
				    	<a href="">Read More</a>
				    </div>
			    </div>
			    <div class="slide2div">
			    	<div class="blog-container">
						<div class="blogimg">
							<a href=""><img src="<?php echo base_url();?>assets/images/blog2_1.png" alt="Avatar" class="image" style="width:100%"></a>
						</div>  
				    </div>
			      	<div class="blog-content">
				    	<h3>How to ROCK A BOY</h3>
				    	<h5 class="time-conment"><i class="fa fa-calendar"></i>&nbsp; 20 - Jun</h5>
				    	<p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat</p>
				    	<a href="">Read More</a>
				    </div>
			    </div>
			</section>
		</div>	
	</div>
</section>
