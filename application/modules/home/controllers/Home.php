<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Home extends CI_Controller 
{
	
	function __construct(){
		parent::__construct();
		
		$this->load->model('homemodel');
		
		
	}
	
	function index() {
	
		$data['menu'] = $this->homemodel->getMenuData();
		$data['category'] = $this->homemodel->getsubMenuData();
		$data['concern'] = $this->homemodel->getconcerns();
		$data['ethos'] = $this->homemodel->getethos();
		
		
		$data['mainslider'] = $this->homemodel->mainslider();
		$data['featured'] = $this->homemodel->getFeaturedProd();
		$data['best_seller'] = $this->homemodel->getBestSellerProd();
		$data['trending'] = $this->homemodel->getTrendingProd();
		$data['hotdeals'] = $this->homemodel->getHotdeals();
		
		
		if(isset($_SESSION['cart_id'])){
			
			$cart_id = $this->session->cart_id;
			$data['cart'] = $this->homemodel->getCartdata($cart_id);
			
		}else{
			
			$data['cart'] = false;
			
		}
		
		$this->load->view('includes/header',$data);
		$this->load->view('home/index');
		$this->load->view('includes/footer');
		
	}
	
	
	function addtocart(){
		
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
			{    
		
				if(!isset($_SESSION['cart_id'])){
					$this->session->set_userdata('cart_id','SKED-temp-cart'.generateRandomString());
					$this->session->set_userdata('is_logged_in', 'false');	
				}
				
				$prod_id = $this->input->post('prd');
				$quantity = $this->input->post('qty');
				
				if($this->homemodel->checkToCart($prod_id , $quantity)){
					
					$this->homemodel->updateCart($prod_id , $quantity);
					
				}else{
					
					$this->homemodel->addTocart($prod_id , $quantity);
					
				}
				
				$data['cart'] = $this->homemodel->getCartdata($_SESSION['cart_id']);
				$this->load->view('home/updcart',$data);
				
				
				
			
			}else{
				
				header('location: '.base_url());
			}
		
	}
	
	
	
}

?>
