<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class cart extends CI_Controller 
{
	
	function __construct(){
		parent::__construct();
		
		$this->load->model('home/homemodel');
		$this->load->database();
		
	}
	
	function index() {
		
		$data['menu'] = $this->homemodel->getMenuData();
		$data['category'] = $this->homemodel->getsubMenuData();
		$data['concern'] = $this->homemodel->getconcerns();
		$data['ethos'] = $this->homemodel->getethos();
		
		
		if(isset($_SESSION['cart_id'])){
			
			$cart_id = $this->session->cart_id;
			$data['cart'] = $this->homemodel->getCartdata($cart_id);
			
		}else{
			
			$data['cart'] = false;
			
		}
		
		$this->load->view('includes/header',$data);
		$this->load->view('cart/cart',$data);
		$this->load->view('includes/footer');
		
	}
    function cartUpdate()
    {
      
      $qty=$this->input->post('qty');
      $id=$this->input->post('id');
      
      $data=array('quantity'=>$qty);
      
      $this->db->where('cart_id',$id);
      $res=$this->db->update('tbl_cart',$data);
      echo json_encode($res);
    }
	function cartCupon()
    {
      $sesid=$this->input->post('sesid');  
      $cupon=$this->input->post('cupon');
      if($cupon=='abc')
      {
        $data=array('type'=>'false','msg'=>'Invalid Coupon code');
        echo json_encode($data);
      }
      if($cupon=='123')
      {
        $data=array('type'=>'true','msg'=>76);
        echo json_encode($data);
      }
    }
	function cartremove()
    {
        $id=$this->input->post('id');
        $this->db->where('cart_id',$id);
        $result=$this->db->update('tbl_cart',array('is_deleted'=>1));
        echo json_encode($result);
    }
    function test()
    {
       ob_start();
        error_reporting( 0 );
        ini_set('display_errors', 'off');
        $parentusername = 'parent test'; // parent username
        $Type = 'Others'; // Proprietorship , Partnership_Firm , Pvt_Ltd_or_LLP , Others
        $company_name = 'Velociter Solution'; // client company name
        $website = 'google.com'; //client firm website name
        $address1 = '12 buglo kate chowk shanti nagar vikram corner'; // client address 1 details 35 character limit
        $address2 = '112 buglo kate chowk shanti nagar vikram corner '; // client address 2 details 35 character limit
        $landmark = '112 buglo kate chowk shanti nagar vikram corner'; // client address landmark details 35 character limit
        $pincode = '110001'; // client pincode details
        $country_name = 'india'; // client country name
        $state_name = 'mahrashtra'; // client state name
        $city_name = 'pune'; // client city name
        $firstname = 'rakesh'; // client firstname
        $lastname = 'rakesh'; // client lastname
        $contact_person='manoj'; // contact person name
        $email='rahul.velociters@gmail.com'; // client email ID means your username
        $mobile = '9960174237'; // client contact number
        $bank_name = 'ICICI'; // client bank details
        $bankbranchname = 'ICICI'; // client bank branch details
        $account_number = '234234234'; // bank details optional
        $accountholdername = 'rakesh'; // acoount holder name
        $ifccode = '876786868'; // bank details optional
        $account_type = 'saving'; // account type will be saving / current optional
        $pan_number='234234234'; // add pan number here but upload from website
        $addressproof = 'license'; // add address proof name but upload from website
        $incorporatecertificate = '123'; // add incorporate certificate name but upload from website
        $license = 'KJHJKNF'; // add license number herer and upload from dashboard
        $servicetaxnumber='234234234'; // add service tax number herer and upload from dashboard
        $vatinnumber= '234234234'; // add vat in number herer and upload from dashboard
        $cheque='234234234'; // add cheque number herer and upload from dashboard
        $gstin='234234234';
        $request_url ='http://seller.shipyaari.com/logistic/webservice/Registration.php';
        $post_data ='gstin='.urlencode( base64_encode($gstin)).' &type='.urlencode(
        base64_encode($Type)).'&website='.urlencode( base64_encode($website)).'&bankbranchname='.urlencode(
        base64_encode($bankbranchname)).'&accountholdername='.urlencode(
        base64_encode($accountholdername)).'&addressproof='.urlencode(
        base64_encode($addressproof)).'&cheque='.urlencode( base64_encode($cheque)).'&vatinnumber='.urlencode(
        base64_encode($vatinnumber)).'&license='.urlencode( base64_encode($license)).'&incorporatecertificate='.urlencode(
        base64_encode($incorporatecertificate)).'&parentusername='.urlencode(
        base64_encode($parentusername)).'&parent_id='.urlencode( base64_encode($parent_id)).'&username='.urlencode(
        base64_encode($username)).'&firstname='.urlencode( base64_encode($firstname)).'&lastname='.urlencode(
        base64_encode($lastname)).'&contact_person='.urlencode( base64_encode($contact_person)).'&mobile='.urlencode(
        base64_encode($mobile)).'&email='.urlencode( base64_encode($email)).'&bank_name='.urlencode(
        base64_encode($bank_name)).'&account_number='.urlencode(
        base64_encode($account_number)).'&account_type='.urlencode( base64_encode($account_type)).'&ifc_code='.urlencode(
        base64_encode($ifccode)).'&company_name='.urlencode( base64_encode($company_name)).'&address='.urlencode(
        base64_encode($address1)).'&address2='.urlencode( base64_encode($address2)).'&landmark='.urlencode(
        base64_encode($landmark)).'&country_name='.urlencode( base64_encode($country_name)).'&state_name='.urlencode(
        base64_encode($state_name)).'&city_name='.urlencode( base64_encode($city_name)).'&pincode='.urlencode(
        base64_encode($pincode)).'&vatin_number='.urlencode(
        base64_encode($vatin_number)).'&servicetaxnumber='.urlencode(
        base64_encode($servicetaxnumber)).'&pan_number='.urlencode(
        base64_encode($pan_number)).'&tan_number='.urlencode( base64_encode($tan_number)).'';
        
        $post = curl_init();
        curl_setopt($post, CURLOPT_URL, $request_url);
        curl_setopt($post, CURLOPT_POST,TRUE);
        curl_setopt($post, CURLOPT_POSTFIELDS, $post_data);
        curl_setopt($post, CURLOPT_RETURNTRANSFER, TRUE);
        $response = curl_exec($post);
        
        print_r($response);
        $result = json_decode($response);
        print_r($result);
    }
}

?>
