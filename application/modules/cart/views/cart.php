
</header>
<style>
input[type='number'] {
    -moz-appearance:textfield;
}

input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
    -webkit-appearance: none;

}
</style>
<section class="maincontent">
	<div class="container">
		<div class="row">
			<div class="cart-title">
				<h2>Shopping Cart</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-md-9">
				<div class="table-wrapper">
					<table>
						<thead>
							<tr style="border-top: 0px;">
								<th>ITEM</th>
								<th>Price</th>
								<th>QTY</th>
								<th>Subtotal</th>
							</tr>
						</thead>
						
						<tbody class="carttable">
							
                                <?php
                                $total=0;
                                $shipcharge=400;
                                  if($cart!=false)
                                  {
                                    
                                    foreach($cart as $key=>$val)
                                    {
                                        $total+=$val['quantity']*$val['price'];
                                       echo '<input type="hidden" value="'.$val['customer_cart_id'].'" id="session" />'; 
                                      echo '<tr>
                                                <td class="itemdetailcart">
                									<img src="'.base_url($val['images']).'" class="cartimg">
                									<div class="product-title-cart">
                										<a href=""><span class="cart-title1">'.$val['product_name'].'</span></a>
                									</div>
                								</td>
                								<td>'.$val['price'].'</td>
                								<td><input id="qty" onblur="qtyUpdate(this)" cartid="'.$val['cart_id'].'" type="number" value="'.$val['quantity'].'" style="width:50px;"></td>
                								<td>'.$val['quantity']*$val['price'].' Rs</td>
                                                <td colspan="4">
                									<i class="fa fa-trash"></i>&nbsp;<a onclick="removeCart(this)" cartid="'.$val['cart_id'].'">Remove item</a>
                								</td>
                                            </tr>';  
                                    }
                                    
                                  }
                                  else
                                  {
                                    echo '<h3>No Date here<h3/>';
                                  }
                                ?>
								
							

						</tbody>
					</table>
				</div>	
			</div>
			<div class="col-md-3">
				<div class="summary">
					<h2 class="summary-title">Summary</h2>
					<hr>
					<p style="padding: 0px 0px 20px;">
						<span class="leftsummary">Subtotal</span>
						<span class="rightsummary"><?php echo $total;?> Rs</span>
                        
					</p>
                    <p style="padding: 0px 0px 20px;">
                        <span>Discount</span>
                        <span class="rightsummary" id="cuponDisc">0</span>
                    </p>
					<p>
                        <span id="coupondisc"></span>
                        <?php 
                           if($total>0)
                           {
                              if($total<$shipcharge)
                              {
                                echo '<span class="leftsummary bold">Shipping</span><span> (Flat Rate - Fixed)</span>
    						         <span class="rightsummary">'.$shipcharge.' Rs</span>';            
                              }
                              else
                              {
                                $shipcharge=0;
                                 echo '<span class="leftsummary bold">Free Shipping';
    						  }    
                           }
                           else
                           {
                             $shipcharge=0;
                           } 
                        ?>
						
					</p>
					<hr>
					<p>
						<span class="leftsummary">Order Total</span>
						<span id="ordtotal" class="rightsummary bold"><?php
                        $result=$total+$shipcharge;     
                        echo $result; ?> Rs</span>
					</p><br>
					<hr>
					<p id="open-discount">Apply Discount Code <i class="fa fa-angle-up pull-right"></i></p>
					<br>
					<div id="discount-code" class="animated slideIn">
						<label>Enter discount code</label>
						<input type="text" name="discount" id="discount" class="form-control" />
                        <p id="msg" style="color:red;"></p>
						<br>
						<button class="btn btn-signbtn" id="cupon">Check Coupon code</button>
					</div>
					<br>
					<a href="checkout.php"><button class="btn btn-signbtn">Proceed to checkout</button></a>
				</div>
			</div>
		</div>
		<div class="row cartbtn1">
			<a href="index.php" ><button class="btn btn-signbtn cartbtn">continue shopping</button></a>&nbsp;&nbsp;
			<button class="btn btn-signbtn cartbtn">clear shopping cart</button>&nbsp;&nbsp;
			<button class="btn btn-signbtn cartbtn">upadate shopping cart</button>
		</div>
	</div>
</section>

<script type="text/javascript">
$('#cupon').click(function()
{
    var cupon=$('#discount').val().trim();
     var id=$('#session').val();
    $.ajax({
        url:'<?php echo base_url('cart/cartCupon'); ?>',
        type:'POST',
        data:{'cupon':cupon,'sesid':id},
        dataType: 'json',
        success: function(data) 
        {
          if(data.type=='false')
          {
            $('#msg').text(data.msg)
          }
          else
          {
             var total='<?php echo $total;?>';
             var ship='<?php echo $shipcharge;?>';
             var disc=data.msg;
             var result=parseInt(total)+parseInt(ship);
             $('#cuponDisc').text('- '+disc+' Rs')
             $('#ordtotal').text(result-disc)
          }
          
        }
    }); 
    
});
$('#open-discount').on("click", function(){
	 $("#discount-code").slideToggle(500);
});
function qtyUpdate(obj)
{
    var id=$(obj).attr('cartid');
    var qty=$(obj).val();
    if(qty=='0')
    {
      qty=1;
    }
    
   $.ajax({
        url:'<?php echo base_url('cart/cartUpdate'); ?>',
        type:'POST',
        data:{'qty':qty,'id':id},
        dataType: 'json',
        success: function(data) 
        {
          window.location.href="";
        }
    }); 
}
function removeCart(obj)
{
    var id=$(obj).attr('cartid');
    $.ajax({
        url:'<?php echo base_url('cart/cartremove'); ?>',
        type:'POST',
        data:{'id':id},
        dataType: 'json',
        success: function(data) 
        {
          window.location.href="";
        }
    });
}
</script>
