<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Skineddict Shoppe</title>
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="author" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link rel="shortcut icon" href="favicon.jpg" type="image/x-icon"/>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  
	<link rel="stylesheet"  href="<?php echo base_url(); ?>assets/css/slick.css">
	<link rel="stylesheet"  href="<?php echo base_url(); ?>assets/css/slick-theme.css">
	<link href="<?php echo base_url(); ?>assets/css/custom.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Yesteryear" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/animate.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/responsive.css">
	 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

	 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.7.5/css/bootstrap-select.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.7.5/js/bootstrap-select.min.js"></script>

</head>
<body>

	<header>
		<div class="hidden-md hidden-lg toplink" id="responsiveheader">
				<p ><a href=""><i class="fa fa-phone"></i> (08) 123 456 7890</a></p>
				<p ><a href=""><i class="fa fa-envelope-open"></i> info@skineddict.com</a></p>
			<p class="text-center">
				<span class="resicon"><a href="https://www.facebook.com/skineddictshoppe/"><i class="fa fa-facebook"></i></a></span>
				<span class="resicon"><a href="https://www.instagram.com/skineddict/"><i class="fa fa-instagram"></i></a></span>
				<!-- <span class="resicon"><a href=""><i class="fa fa-twitter"></i></a></span>
				<span class="resicon"><a href=""><i class="fa fa-linkedin"></i></a></span> -->
			</p>
		</div>
	<div class="main-header hidden-sm hidden-xs">
		<div class="container ">
			<div class="row toplink">
				<ul class="nav navbar-nav">
					<li><a href=""><i class="fa fa-phone"></i> (08) 123 456 7890</a></li>
					<li><a href=""><i class="fa fa-envelope-open"></i> info@skineddict.com</a></li>
					<li><a href="https://www.facebook.com/skineddictshoppe/"><i class="fa fa-facebook"></i></a></li>
					<li><a href="https://www.instagram.com/skineddict/"><i class="fa fa-instagram"></i></a></li>
				</ul>
				 <ul class="nav navbar-nav navbar-right">
	                <li>
	                	<div class="logindropdown">
						  <i class="fa fa-user-circle loginbtndrp"> <?php if(isset($_SESSION['username'])){ echo ucfirst($_SESSION['username']);}else{ echo "Login";} ?>  </i><i class="fa fa-angle-down"></i>
						  <div class="logindropdown-content">
						  	<a href="wishlist.php">My Wishlist</a>
						  	<a href="compare.php">My Compare</a>
						    <?php if(isset($_SESSION['is_logged_in']) && $_SESSION['is_logged_in'] == "true"){?> <a href="<?php echo base_url(); ?>account/logout"> Logout </a><?php }else{?> <a href="<?php echo base_url(); ?>account/login"> Login </a> <?php } ?>
						    <a href="register.php">My Accounts</a>
						  </div>
						</div>
	                </li>
	                <li>
	                	<div class="logindropdown">
						  <span class="loginbtndrp">INR </span><i class="fa fa-angle-down"></i>
						  <div class="logindropdown-content">
						    <a href="#">EUR</a>
						  </div>
						</div>
	            </ul>
			</div>
		</div>
	</div>
	<nav class="navbar navbar-fixed-top usernav">
    <div class="container">
    	<div class="row">
    		 <!-- Brand and toggle get grouped for better mobile display -->
	        <div class="navbar-header">
	            <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
	                <span class="sr-only">Toggle navigation</span>
	                <span class="icon-bar"></span>
	                <span class="icon-bar"></span>
	                <span class="icon-bar"></span>
	            </button>
	            <a href="#" class="pull-right hidden-md hidden-lg" style="padding: 10px 10px;"><i class="fa fa-search secnav"></i></a>
	            <a href="cart.php" class="pull-right hidden-md hidden-lg" style="padding: 10px 10px;"><i class="fa fa-shopping-cart secnav"></i></a>
	            <a href="index.php" class="navbar-brand"><img src="<?php echo base_url();?>assets/images/logo.png"></a>
	        </div>
	        <!-- Collection of nav links and other content for toggling -->
	        <div id="navbarCollapse" class="collapse navbar-collapse">
	            <ul class="nav navbar-nav centernav">
				<?php foreach($menu as $key=>$value){ ?>
	            	<li><a href="listing.php"><?php echo $value['category']; ?><i class="fa fa-angle-down"></i></a>
	            		<ul class="dropdown nav-folderized">
	            			<div class="container">
	            				<div class="row">
				            		<div class="col-md-4 ftr__list">
				            			<h6 class="dropdownheading">CONTENT BY CATEGORY</h6>
				            			<ul class="anotherul">
												<?php foreach($category as $k=>$v){ 
												
												if($v['cat_id'] == $value['cat_id']){
												?>
												
												<li><a href="<?php echo base_url();?>products/category/<?php echo $v['subcat_id']; ?>/<?php echo $value['cat_id']; ?>"><?php echo $v['subcategory']; ?></a></li>
												
												<?php 
												}
												} ?>
												
				            				
				            			</ul>
				            		</div>
				            		<div class="col-md-4 ftr__list">
									<?php
									if(array_search($value['cat_id'], array_column($concern, 'category_id')) !== false){  ?>
				            			<h6 class="dropdownheading">CONTENT BY CONCERN</h6>
				            			<ul class="anotherul">
											<?php foreach($concern as $k=>$v){ 
												
												if($v['category_id'] == $value['cat_id']){
												?>
												
												<li><a href="<?php echo base_url();?>products/concern/<?php echo str_replace(" ","-", strtolower($v['concern'])); ?>/<?php echo $value['cat_id']; ?>"><?php echo $v['concern']; ?></a></li>
												
												<?php 
												}
												} ?>
				            			</ul>
										
									<?php } ?>
				            		</div>
				            		<div class="col-md-4 ftr__list">
																		<?php
									if(array_search($value['cat_id'], array_column($ethos, 'category_id')) !== false){  ?>
				            			<h6 class="dropdownheading">CONTENT BY ETHOS</h6>
				            			<ul class="anotherul">
											<?php foreach($ethos as $k=>$v){ 
												
												if($v['category_id'] == $value['cat_id']){
												?>
												
												<li><a href="<?php echo base_url();?>products/ethos/<?php echo str_replace(" ","-",strtolower($v['ethos'])); ?>/<?php echo $value['cat_id']; ?>"><?php echo $v['ethos']; ?></a></li>
												
												<?php 
												}
												} ?>
				            			</ul>
										
									<?php } ?>
				            		</div>
				            	</div>
	            			</div>
			            </ul>
	            	</li>
					
					<?php } ?>
	            	
	            	
	            	<li class="hidden-md hidden-lg"><a href="login.php">Sign In</a></li>
					<li class="hidden-md hidden-lg"><a href="register.php">My Accounts</a></li>
	            </ul>
	            <ul class="nav navbar-nav navbar-right" style="margin: 15px 0px;">
	                <li class="hidden-xs hidden-sm"><a href="#" style="padding: 10px 20px;"><i class="fa fa-search secnav"></i></a></li>
	                <li class="hidden-xs hidden-sm">
	                	<div class="cartheader" style="margin: 10px 0px;">
						    <span class="cartbtn" style="padding: 10px 20px;"><i class="fa fa-shopping-cart secnav"> </i>
						    </span>
						    <div class="dropdown-cart">
						    	<p style="font-size: 11px;">
						    		<span><?php if($cart){echo count($cart);}else{ echo 0;} ?></span><span> items</span>
						    		<?php 
									$total = 0;
									if($cart){
										foreach($cart as $kkk=>$vvv) {
											
											$total += $vvv['price']*$vvv['quantity'];
											
										}
									}?>
						    		<span style="padding: 0px 4px 0px 15px; font-size: 10px;">CART SUBTOTAL</span><span> €<?php echo $total; ?></span>
						    	</p>
						    	<a href="checkout.php"><button class="btn btn-checkobtn">Go to checkout</button></a>
						    	<div class="minicart-items-wrapper">
						    		<ol style="list-style: none; margin-left: -30px;">
									
									
										<?php if($cart){
											
											foreach($cart as $kk1=>$vv1){ ?>
												
												<li>
						    				<span><img src="<?php echo base_url().$vv1['images'];?>" class="minicart-img"></span>
											<div class="cart-product-detail">
												<p class="mini-cart-title"><a href="#"><?php echo $vv1['product_name'];?>?</a></p>
												<p><span>Qty: </span><span><?php echo $vv1['quantity']; ?></span></p>
												<p><span>€<?php echo $vv1['price']*$vv1['quantity']; ?></span></p>
											</div>
												</li>
												<hr>
												
											<?php	
											}
										}else{
											
											echo "Your shopping cart is empty!";
											
										}
										?>
										
										
						    		</ol>
						    	</div>
						    	<a href="<?php echo base_url('cart');?>" style="padding: 0px !important;"><button class="btn btn-minicartbtn">view and edit cart</button></a>
						    </div>
						</div>
					</li>
	            </ul>
	        </div>
    	</div>
    </div>
</nav>

<script type="text/javascript">
	  jQuery(".nav-folderized h6").click(function(){
	  jQuery(this).parent(".ftr__list").toggleClass("open"); 
	  jQuery('html, body').animate({ scrollTop: jQuery(this).offset().top - 170 }, 1500 );
  });
</script>