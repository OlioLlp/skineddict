<section id="brand-slider">
	<div class="container">
		<div class="row">
			<section class="brandslider slider" id="brandslider1">
				<div style="padding: 0px 20px;">
					<img src="<?php echo base_url();?>assets/images/brands/1.-M.A.png" class="img-responsive">
			    </div>
			    <div style="padding: 0px 20px;">
			    	<img src="<?php echo base_url();?>assets/images/brands/2.png" class="img-responsive">
			    </div>
			    <div style="padding: 0px 20px;">
			    	<img src="<?php echo base_url();?>assets/images/brands/3.png" class="img-responsive">
			    </div>
			    <div style="padding: 0px 20px;">
			    	<img src="<?php echo base_url();?>assets/images/brands/4.png" class="img-responsive">
			    </div>
			    <div style="padding: 0px 20px;">
			    	<img src="<?php echo base_url();?>assets/images/brands/6.png" class="img-responsive">
			    </div>
			</section>
		</div>
	</div>
</section>

<section id="block-static">
	<div class="box-static static1">
		<div class="mainbox">
			<div class="topbox">
				<img src="<?php echo base_url();?>assets/images/foo-static1.png" class="img-responsive">
			</div>
			<div class="desbox">
				<h3 class="animated bounceInDown">NOW INTRODUCING</h3>
				<a href="" class="animated zoomIn">Shop Now</a>
				<h1 class="animated bounceInLeft">Spa Optima+</h1>
			</div>
		</div>
	</div>
	<div class="box-static static2">
		<div class="mainbox">
			<div class="topbox">
				<img src="<?php echo base_url();?>assets/images/foo-static2.png" class="img-responsive">
			</div>
			<div class="desbox">
				<h3 class="animated bounceInDown">WRINKLE CURE</h3>
				<a href="" class="animated zoomIn">Shop Now</a>
				<h1 class="animated bounceInLeft">Time Revolution</h1>
			</div>
		</div>
	</div>
	<div class="box-static static3">
		<div class="mainbox">
			<div class="topbox">
				<img src="<?php echo base_url();?>assets/images/foo-static3.png" class="img-responsive">
			</div>
			<div class="desbox">
				<h3 class="animated bounceInDown">PRETTY PERKS FOR EVERY POINT YOU EARN</h3>
				<a href="" class="animated zoomIn">Shop Now</a>
				<h1 class="animated bounceInLeft">Beauty Squad</h1>
			</div>
		</div>
	</div>
</section>
<footer id="footerhead">
	<div class="container">
		<div class="row">
			<div class="footer-main hidden-sm hidden-xs">
				<div class="col-md-3">
					<h2>ABOUT SKINEDDICT SHOPPE</h2>
					<p>We bring you 100% Genuine skincare & makeup products from around the world, understanding Skin and getting you the products that work!! forcing friends and family to be more active in their regime.</p>
					<div class="socialweblink">
						<ul>
							<li><a href="https://www.facebook.com/skineddictshoppe/"><i class="fa fa-facebook"></i></a></li>
							<li><a href="https://www.instagram.com/skineddict/"><i class="fa fa-instagram"></i></a></li>
							<!-- <li><a href=""><i class="fa fa-twitter"></i></a></li>
							<li><a href=""><i class="fa fa-pinterest"></i></a></li>
							<li><a href=""><i class="fa fa-google-plus"></i></a></li> -->
						</ul>
					</div>
				</div>
				<div class="col-md-3">
					<h2>INFORMATION</h2>
					<div class="multilinks">
						<ul>
							<li><a href="">Returns</a></li>
							<li><a href="">Delivery</a></li>
							<li><a href="">Services</a></li>
							<li><a href="">Gift Cards</a></li>
							<li><a href="">Unsubscribe Notification</a></li>
						</ul>
					</div>
				</div>
				<div class="col-md-3">
					<h2>OUR OFFERS</h2>
					<div class="multilinks">
						<ul>
							<li><a href="">New Products</a></li>
							<li><a href="">Top Sellers</a></li>
							<li><a href="">Specials</a></li>
							<li><a href="">Manufacturers</a></li>
							<li><a href="">Suppliers</a></li>
						</ul>
					</div>
				</div>
				<div class="col-md-3">
					<h2>GET IN TOUCH</h2>
					<div class="addressfooter">
						<ul>
							<li><span class="addicon"><i class="fa fa-home"></i></span> <span class="adddetails" style="margin-top: -30px;">Lokhandwala Complex Andheri</span></li>
							<li><span class="addicon"><i class="fa fa-phone"></i></span> <span class="adddetails">(08) 123 456 7890</span></li>
							<li><span class="addicon"><i class="fa fa-envelope-open"></i></span> <span class="adddetails">info@skineddict.com</span></li>
							<li><img src="<?php echo base_url();?>assets/images/payment.png" style="margin: 10px 0px;"></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="footer-main hidden-md hidden-lg">
				<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="headingOne">
							<h4 class="panel-title">
								<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
									<i class="more-less fa fa-angle-up pull-right"></i>
									<h2>ABOUT SKINEDDICT SHOPPE</h2>
								</a>
							</h4>
						</div>
						<div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
							<div class="panel-body">
								<p>We bring you 100% Genuine skincare & makeup products from around the world, understanding Skin and getting you the products that work!! forcing friends and family to be more active in their regime.</p>
								<div class="socialweblink">
									<ul>
										<li><a href="https://www.facebook.com/skineddictshoppe/"><i class="fa fa-facebook"></i></a></li>
										<li><a href="https://www.instagram.com/skineddict/"><i class="fa fa-instagram"></i></a></li>
										<!-- <li><a href=""><i class="fa fa-twitter"></i></a></li>
										<li><a href=""><i class="fa fa-pinterest"></i></a></li>
										<li><a href=""><i class="fa fa-google-plus" aria-hidden="true"></i></a></li> -->
									</ul>
								</div>
							</div>
						</div>
					</div>

					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="headingTwo">
							<h4 class="panel-title">
								<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
									<i class="more-less fa fa-angle-up pull-right"></i>
									<h2>INFORMATION</h2>
								</a>
							</h4>
						</div>
						<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
							<div class="panel-body">
								<div class="multilinks">
									<ul>
										<li><a href="">Returns</a></li>
										<li><a href="">Delivery</a></li>
										<li><a href="">Services</a></li>
										<li><a href="">Gift Cards</a></li>
										<li><a href="">Unsubscribe Notification</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>

					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="headingThree">
							<h4 class="panel-title">
								<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
									<i class="more-less fa fa-angle-up pull-right"></i>
									<h2>OUR OFFERS</h2>
								</a>
							</h4>
						</div>
						<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
							<div class="panel-body">
								<div class="multilinks">
									<ul>
										<li><a href="">New Products</a></li>
										<li><a href="">Top Sellers</a></li>
										<li><a href="">Specials</a></li>
										<li><a href="">Manufacturers</a></li>
										<li><a href="">Suppliers</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>

					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="heading4">
							<h4 class="panel-title">
								<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="false" aria-controls="collapse4">
									<i class="more-less fa fa-angle-up pull-right"></i>
									<h2>GET IN TOUCH</h2>
								</a>
							</h4>
						</div>
						<div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4">
							<div class="panel-body">
								<div class="addressfooter">
									<ul>
										<li><span class="addicon"><i class="fa fa-home"></i></span> <span class="adddetails" style="margin-top: -30px;">Lokhandwala Complex Andheri</span></li>
										<li><span class="addicon"><i class="fa fa-phone"></i></span> <span class="adddetails">(08) 123 456 7890</span></li>
										<li><span class="addicon"><i class="fa fa-envelope-open"></i></span> <span class="adddetails">info@skineddict.com</span></li>
										<li><img src="<?php echo base_url();?>assets/images/payment.png" style="margin: 10px 0px;"></li>
									</ul>
								</div>
							</div>
						</div>
					</div>

					</div><!-- panel-group -->	
			</div>
			<div id="footer-bottom">	
				<!-- <div class="row"> -->
					<div class="col-md-6">
						<div class="footer-cpoyright">
							<span class="copyright">© Skineddict Shoppe</span>
						</div>
					</div>
					<div class="col-md-6">
						<div class="footerlogo"><img src="<?php echo base_url();?>assets/images/logo.png"></div>
					</div>
				<!-- </div> -->
			</div>
		</div>
	</div>


<div id="toTop"><i class="fa fa-angle-double-up"></i></div>
<script type="text/javascript">
	$(window).scroll(function() {
    if ($(this).scrollTop()) {
        $('#toTop').fadeIn();
    } else {
        $('#toTop').fadeOut();
    }
});

$("#toTop").click(function () {
   $("html, body").animate({scrollTop: 0}, 1000);
});
</script>

<script type="text/javascript">
	function toggleIcon(e) {
    $(e.target)
        .prev('.panel-heading')
        .find(".more-less")
        .toggleClass('fa-angle-up fa-angle-down');
}
$('.panel-group').on('hidden.bs.collapse', toggleIcon);
$('.panel-group').on('shown.bs.collapse', toggleIcon);
</script>

</footer>

<script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/slick.js" type="text/javascript" charset="utf-8"></script>
<script src="<?php echo base_url(); ?>assets/js/slider.js"></script>
<script src="<?php echo base_url(); ?>assets/js/ske.js"></script>



</body>
</html>