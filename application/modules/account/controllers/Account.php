<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Account extends CI_Controller 
{
	
	function _remap($method_name = 'index')
	{ 
        if(!method_exists($this, $method_name))
		{ 
			$this->index(); 
        } 
        else
		{
			$this->{$method_name}(); 
        } 
    }
	
	function __construct(){
		parent::__construct();
		
		$this->load->model('accountmodel');
		$this->load->model('home/homemodel');
	}
	
	function index() {
		
		
	}
	
	function login(){
		
		if(!isset($_SESSION['user_id'])){
		/*For Mega menu Bar*/
		$data['menu'] = $this->homemodel->getMenuData();
		$data['category'] = $this->homemodel->getsubMenuData();
		$data['concern'] = $this->homemodel->getconcerns();
		$data['ethos'] = $this->homemodel->getethos();
		
		/*For cart*/
		if(isset($_SESSION['cart_id'])){
			
			$cart_id = $this->session->cart_id;
			$data['cart'] = $this->homemodel->getCartdata($cart_id);
			
		}else{
			
			$data['cart'] = false;
			
		}
		
		
		
		$this->load->view('includes/header',$data);
		$this->load->view('account/login');
		$this->load->view('includes/footer');
		
		
		}else{
			
			header('location: '.base_url());
		}
	}
	
	function logout(){
		
		$this->session->sess_destroy();
		header('location: '.base_url());
	}
	
	function register(){
		
		/*For Mega menu Bar*/
		$data['menu'] = $this->homemodel->getMenuData();
		$data['category'] = $this->homemodel->getsubMenuData();
		$data['concern'] = $this->homemodel->getconcerns();
		$data['ethos'] = $this->homemodel->getethos();
		
		$this->load->view('includes/header',$data);
		$this->load->view('account/register');
		$this->load->view('includes/footer');
		
		
	}
	
	function test(){
		
		
		echo $orderNumber = "SKED-cart".generateRandomString();
	}
	
	function add_new_customer(){
		
		
		if($this->input->post('password') !== null){
			$p1 = $this->input->post('password');
			$p2 = $this->input->post('cpassword');
			
			if($p1 == $p2){
			$insert['customer_name'] = $this->input->post('fname');
			$insert['last_name'] = $this->input->post('lname');
			$insert['email'] = $this->input->post('email');
			$insert['mobile'] = $this->input->post('mnum');
			$insert['password'] = md5($this->input->post('password'));
			$insert['cart_id'] = "SKED-cart".generateRandomString();
				if($this->accountmodel->insertInto("tbl_customer",$insert)){
					
					$data['status']="success";
				}else{
					$data['status']="fail";
					
				}
			
			
			}else{
				
				$data['status']="fail";
				
			}
			$this->load->view('account/register_success',$data);
		
		}else{
			header('location: '.base_url().'account/register');
		}
		
		
		
	}
	
	function checkEmail(){
		
		$email = $this->input->post('email');
		echo $this->accountmodel->validateEmail($email);
		
	}
	
	
	function logged_in(){
		
		$email = $this->input->post('email');
		$password = md5(($this->input->post('password')));
		
		$result = $this->accountmodel->loinmodel($email,$password);
		
		if(!$result){
			echo json_encode(array("success"=>false,'msg'=>'unsuccessfull'));
			exit;
			
			
		}else{
			
			/*Add products after login*/
			$previous_cart = "";
			if(isset($_SESSION['cart_id'])){
				
				$previous_cart = $_SESSION['cart_id'];
				
			}
			
			$this->session->set_userdata('cart_id', $result[0]['cart_id']);
			$this->session->set_userdata('is_logged_in', 'true');
			$this->session->set_userdata('user_id', $result[0]['customer_id']);
			$this->session->set_userdata('username', $result[0]['customer_name']);
			
			
			if($previous_cart != ""){
				if($cartdata = $this->homemodel->getCartdata($previous_cart)){
					
					
					foreach($cartdata as $key=>$value){
						
						if($this->homemodel->checkToCart($value['product_id'],$value['quantity'])){
							
							$this->homemodel->updateQuantity($value['product_id'],$value['quantity'],$previous_cart);
							
						}else{
							
							$this->homemodel->updateCartId($value['product_id'],$value['quantity'],$previous_cart);
							
						}
						
					}
					
				}
			}
			echo $previous_cart;
			//echo json_encode(array("success"=>true,'msg'=>'successfull'));
			//exit;
			
		}
		
		
		
		
	}
	
	function forgot_password(){
		
		/*For Mega menu Bar*/
		$data['menu'] = $this->homemodel->getMenuData();
		$data['category'] = $this->homemodel->getsubMenuData();
		$data['concern'] = $this->homemodel->getconcerns();
		$data['ethos'] = $this->homemodel->getethos();
		
		$this->load->view('includes/header',$data);
		$this->load->view('account/restpwd');
		$this->load->view('includes/footer');
		
		
	}
	
	
	
}

?>
