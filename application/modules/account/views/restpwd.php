
</header>

<section class="maincontent">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 class="login-title">FORGOT YOUR PASSWORD</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="field-note">
					<span class="notespan">Please enter your email address below to receive a password reset link.</span>
				</div>
				<form>
					<div style="margin-bottom: 20px;">
						<div class="field-lable"><label>Email</label><span class="red">*</span></div>
						<input type="email" name="email" class="userinput">
					</div>
				</form>
			</div>
			<div class="col-md-6">
				
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="forgotbtn">
					<a href="register.php"><button class="btn btn-signbtn">Reset my password</button></a>
				</div>
			</div>
			<div class="col-md-6 forgotbtn">
				<a href="login.php" class="forgotback">Go back</a>
			</div>
		</div>
	</div>
</section>

