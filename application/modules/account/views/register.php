
</header>

<section class="maincontent" style="margin-bottom: 40px;">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 class="login-title">CREATE NEW CUSTOMER ACCOUNT</h1>
			</div>
		</div>
		<form action="<?php echo base_url(); ?>account/add_new_customer" method="POST" class="personalform">
		<div class="row">
			<div class="col-md-6">
				
				
					<div style="margin-bottom: 20px;">
						<div class="field-lablereg"><label>First Name </label><span class="red">*</span></div>
						<input type="text" name="fname" id="fname" class="userinput"><br>
						<span class="notespan" style="color:red;display: block;" id="fnamerror"></span>
					</div>
					<div style="margin-bottom: 20px;">
						<div class="field-lablereg"><label>Last Name </label><span class="red">*</span></div>
						<input type="text" name="lname" id="lname" class="userinput">
						<span class="notespan" style="color:red;display: block;" id="lnamerror"></span>
					</div>
					<div style="margin-bottom: 20px;">
						<div class="field-lablereg"><label>Mobile No. </label><span class="red">*</span></div>
						<input type="text" name="mnum" id="mnum" class="userinput">
						<span class="notespan" style="color:red;display: block;" id="numerror"></span>
					</div>
				
			</div>
			<div class="col-md-6">
				
				
					<div style="margin-bottom: 20px;">
						<div class="field-lablereg"><label>Email</label><span class="red">*</span></div>
						<input type="email" name="email" id="email" class="userinput">
						<span class="notespan" style="color:red;display: block;" id="emailerror"></span>
					</div>
					<div style="margin-bottom: 20px;">
						<div class="field-lablereg"><label>Password</label><span class="red">*</span></div>
						<input type="Password" name="password" class="userinput" id="password">
						<span class="notespan" style="display: block;" id="plength">Passwords must be at least 6 characters.</span>
						
					</div>
					<div style="margin-bottom: 20px;">
						<div class="field-lablereg"><label>Confirm Password</label><span class="red">*</span></div>
						<input type="Password" name="cpassword" id="cpassword" class="userinput">
						<span class="notespan" style="color:red;display: block;" id="pwderror"></span>
					</div>
				
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="divbtnreg">
					<button type="submit" class="btn btn-signbtn" style="float: right;">create an account</button>
					<span style="float: left;"><a href="<?php echo base_url(); ?>account/login">Back</a></span>
				</div>
			</div>
		</div>
		</form>
	</div>
</section>

<script>

base_url = "http://localhost/skineddict/";


$("#password").keyup(function(){
	
	var pass = $(this).val();
	if(pass.length < 6){
		$("#plength").html('').append('Password Strength: <h5 style="color:red;display: inline;" >Bad</h5>');
		
	}else{
		$("#plength").html('').append('Password Strength: <h5 style="color:green;display: inline;" >Good</h5>');
		
	}
});

$( "form" ).submit(function( event ) {
	
	event.preventDefault();
	var flag = 0;
	var fname = document.getElementById("fname").value;
	var lname = document.getElementById("lname").value;
	var email = document.getElementById("email").value;
	var password = document.getElementById("password").value;
	var cpassword = document.getElementById("cpassword").value;
	var mobile = document.getElementById("mnum").value;
	
	var name_char=/^[A-Z a-z]+$/;
	var emailchar = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;
	var phoneno = /^\d{10}$/;
	var result = null;
	
	if(fname == '' || !name_char.test(fname)){
		$("#fnamerror").text("! Please enter valid Name");
		flag++;
	}
	if(lname == '' || !name_char.test(lname) ){
		
		$("#lnamerror").text("! Please enter valid Name");
		flag++;
	}
	if(email == '' || !emailchar.test(email) ){
		
		$("#emailerror").text("! Please enter valid Email Id");
		flag++;
	}
	if(mobile == '' || !phoneno.test(mobile) ){
		
		$("#numerror").text("! Please enter valid Mobile number");
		flag++;
	}
	if(password == '' || password.length < 6){
		
		$("#pwderror").text("! Please enter valid Password");
		flag++;
	}
	if(password != cpassword){
		
		$("#pwderror").text("! Password doesn't match");
		flag++;
	}
	if(flag > 0){
		
		return false;
		
	}else{
		
		$.ajax({
			url: base_url+"account/checkEmail",
			method:"POST",
			data:{
				email: email	
			},
			success: function(rdata) {
				console.log(rdata);
				if(rdata != 1){
					$( "form" ).submit();
				}else{
					$("#emailerror").text("");
					$("#emailerror").text("! Email id already exist");
					
				}
				
			}
		});

	}

	
});







</script>
