
</header>

<section class="maincontent">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 class="login-title">CUSTOMER LOGIN</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<h2 class="login-subtitle">REGISTERED CUSTOMERS</h2>
				<hr class="loginhr">
				<div class="field-note">
					<span class="notespan">If you have an account, sign in with your email address.</span>
					<span class="requiredtext">*Required Fields</span>
				</div>
				<form id="loginForm">
					<div style="margin-bottom: 20px;">
						<div class="field-lable"><label>Email</label><span class="red">*</span></div>
						<input type="email" name="email" id="email" class="userinput">
					</div>
					<div style="margin-bottom: 20px;">
						<div class="field-lable"><label>Password</label><span class="red">*</span></div>
						<input type="Password" name="password" id="password" class="userinput">
						<p id="loger" style="color:red;"></p>
					</div>
					
					<div class="divbtn">
					
						<button type="submit" id="submit" class="btn btn-signbtn">Sign in</button>
						
						<span style="float: right;"><a href="<?php echo  base_url(); ?>account/forgot_password">Forgot Your Password?</a></span>
					</div>
				</form>
			</div>
			<div class="col-md-6">
				<h2 class="login-subtitle">NEW CUSTOMERS</h2>
				<hr class="loginhr">
				<span class="notespan">Creating an account has many benefits: check out faster, keep more than one address, track orders and more.</span>
				<div class="newdivbtn">
					<a href="<?php echo  base_url(); ?>account/register"><button class="btn btn-signbtn">create an account</button></a>
				</div>
			</div>
		</div>
	</div>
</section>
<script>


base_url = "http://localhost/skineddict/";

$( "form" ).submit(function( event ) {
	
	event.preventDefault();
	
	var email = document.getElementById("email").value;
	var password = document.getElementById("password").value;
	
	
	$.ajax({
			url: base_url+"account/logged_in",
			method:"POST",
			dataType: 'json',
			data:{
				email: email,
				password:password
			},
			success: function(rdata) {
				
				if(rdata.success == true){
					
					window.location = "<?php echo base_url();?>";
					
				}else{
				
				$("#loger").text("");
					$("#loger").text("Email id or Password is incorect!!");
				
				}
			}
		});
	
	
	
});
</script>

