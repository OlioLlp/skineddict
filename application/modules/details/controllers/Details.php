<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Details extends CI_Controller 
{
	
	function _remap($method_name = 'index')
	{ 
        if(!method_exists($this, $method_name))
		{ 
			$this->index(); 
        } 
        else
		{
			$this->{$method_name}(); 
        } 
    }
	
	function __construct(){
		parent::__construct();
		
		$this->load->model('detailmodel');
		$this->load->model('home/homemodel');
		
	}
	
	function index() {
		
		 
		/*For Product Details*/
		$pId = urlencode($this->uri->segment(2));
		$data['details'] = $this->detailmodel->getProductDetails($pId);
		
		/*For Product Images*/
		$data['images'] = $this->detailmodel->getProductImages($pId);
		
		/*For Related Products*/
		$subcat = $data['details'][0]['subcategory_id'];
		$data['related'] = $this->detailmodel->getRelatedProducts($subcat,$pId);
		
		
		/*For Customer Reviews*/
		$data['reviews'] = $this->detailmodel->getReviews($pId);
		$rating = $this->detailmodel->getAvgrating($pId);
		$data['avgrating'] = round($rating[0]['avgrating']);
		
		/*For Mega menu Bar*/
		$data['menu'] = $this->homemodel->getMenuData();
		$data['category'] = $this->homemodel->getsubMenuData();
		$data['concern'] = $this->homemodel->getconcerns();
		$data['ethos'] = $this->homemodel->getethos();
		
		/*For cart*/
		if(isset($_SESSION['cart_id'])){
			
			$cart_id = $this->session->cart_id;
			$data['cart'] = $this->homemodel->getCartdata($cart_id);
			
		}else{
			
			$data['cart'] = false;
			
		}
		
		
		
		$this->load->view('includes/header',$data);
		$this->load->view('details/detail');
		$this->load->view('includes/footer');
	
	}
	
	
	
}

?>
