<?PHP
class Detailmodel extends CI_Model
{	
	function getProductDetails($id){
		
	
		$this->db->select('*');
		$this->db->from('tbl_products');
		$this->db->where("product_id",$id);
		$this->db->where("status",1);
		$query = $this->db->get(); 
		if($query->num_rows() >= 1){
			return $query->result_array();
		}else{
			return false;
		}
	}
	
	function getProductImages($id){
		
	
		$this->db->select('*');
		$this->db->from('tbl_productimages');
		$this->db->where("product_id",$id);
		$this->db->where("status",1);
		$this->db->order_by('order_by', 'ASC');
		$query = $this->db->get(); 
		if($query->num_rows() >= 1){
			return $query->result_array();
		}else{
			return false;
		}
	}
	
	
	function getRelatedProducts($subcat,$pId){
		
	
		$this->db->select('*');
		$this->db->from('tbl_products');
		$this->db->where("subcategory_id",$subcat);
		$this->db->where_not_in('product_id', $pId);
		$this->db->where("status",1);
		$this->db->order_by('title', 'RANDOM');
		$this->db->limit(6);
		$query = $this->db->get(); 
		if($query->num_rows() >= 1){
			return $query->result_array();
		}else{
			return false;
		}
	}
	
	function getReviews($pId){
		
	
		$this->db->select(array('tbl_reviews.*','tbl_customer.customer_name'));
		$this->db->from('tbl_reviews');
		$this->db->join('tbl_customer', 'tbl_customer.customer_id = tbl_reviews.customer_id');
		$this->db->where("tbl_reviews.product_id",$pId);
		$this->db->where("tbl_reviews.status",1);

		$query = $this->db->get(); 
		if($query->num_rows() >= 1){
			return $query->result_array();
		}else{
			return false;
		}
	}
	
	function getAvgrating($pId){
		
		$this->db->select('AVG(rating) avgrating');
		$this->db->from('tbl_reviews');
		$this->db->join('tbl_customer', 'tbl_customer.customer_id = tbl_reviews.customer_id');
		$this->db->where("tbl_reviews.product_id",$pId);
		$this->db->where("tbl_reviews.status",1);

		$query = $this->db->get(); 
		if($query->num_rows() >= 1){
			return $query->result_array();
		}else{
			return false;
		}
		
		
	}
	
	
	
}
	

?>
