</header>

<section id="main-content">
	<div class="container">
		<div class="row">
			<div class="col-md-5">
				<section class="regularproduct slider" id="productdetailslider">
				
				<?php 
				if(!$images){ 
				?>
					
					<div>
					    <img src="<?php echo base_url();?>assets/images/products/noimage.jpg">
				    </div>
					<?php
				}else{
				foreach($images as $ki=>$vi){ ?>
					<div>
					    <img src="<?php echo base_url().$vi['image'];?>">
				    </div>
					
				<?php }
				}
				?>
				</section>
				<section class="slider slider-nav hidden-xs hidden-sm" id="multiproduct">
					<?php
					if(!$images){ ?>
						<div>
					    <img src="<?php echo base_url();?>assets/images/products/noimage.jpg">
				    </div>
						
					<?php }else{
					foreach($images as $kii=>$vii){ ?>
					<div>
					    <img src="<?php echo base_url().$vii['image'];?>">
				    </div>
					
				<?php } 
					}
				
				?>
					
				    
				</section>
			</div>
			<div class="col-md-7">
				<div class="detailproduct">
					<div class="page-title-wrapper">
						<h1 class="product-title"><?php echo $details[0]['product_name'];?></h1>
					</div>
					<p>
						<div>
							<span class="fa fa-star <?php if($avgrating >=1){ echo "checked";} ?>"></span>
							<span class="fa fa-star <?php if($avgrating >=2){ echo "checked";} ?>"></span>
							<span class="fa fa-star <?php if($avgrating >=3){ echo "checked";} ?>"></span>
							<span class="fa fa-star <?php if($avgrating >=4){ echo "checked";} ?>"></span>
							<span class="fa fa-star <?php if($avgrating >=5){ echo "checked";} ?>"></span>&nbsp;&nbsp;&nbsp;&nbsp;
							<a href=""><span class="reviews-actions"><?php if(!$reviews){echo 0;}else{echo count($reviews);} ?> Review</span></a>
							<span class="reviews-available">Availability: </span><?php if($details[0]['stock']<= 0 ){echo '<span class="product-info-stock-sku"> Out Of Stock';}if($details[0]['stock']>= 10){echo '<span class="product-info-in-stock"> In Stock';}else{ echo '<span class="product-info-stock-sku"> '.$details[0]['stock'].' left'; } ?></span>
						</div>
					</p>
					
					<div class="product-overview">
						<p><?php echo $details[0]['details'];?></p>
					</div>
					<div class="price-box">
					<?php if($details[0]['offer_price'] != ""){ ?>
						<span class="special-price">€110.00</span>
					<?php } ?>
						<span class="<?php echo($details[0]['offer_price'] != "" ? 'old-price' : 'special-price'); ?>">€130.00</span>
					</div>	
					<div class="product-add-form">
						
							<div class="controls1">
								<input type="number" name="quantity" id="quantity" class="" value="1" min="1" max="<?php echo $details[0]['stock']; ?>">
							</div>
							<div class="actionsdetail">
								<button class="btn btn-detailcart" onclick="addtocart(<?php echo $details[0]['product_id']; ?>)"><i class="fa fa-plus"></i>&nbsp; Add to Cart </button>
							</div>
						
					</div>
					<div class="product-social-links">
						<span class="mailto"><i class="fa fa-envelope-o"></i></span>
						<span class="mailto"><i class="fa fa-heart-o"></i></span>
						<span class="mailto"><i class="fa fa-exchange"></i></span>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<ul class="nav nav-pills">
				    <li class="active"><a data-toggle="pill" href="#detail">How To Use</a></li>
				    <li><a data-toggle="pill" href="#review1" class="secpill">Reviews</a></li>
				</ul>
	  
				<div class="tab-content">
				
				
				    <div id="detail" class="tab-pane fade in active">
				      <p class="description">
					  <?php echo $details[0]['how_to_use'];?>
					  
				      	</p>
				    </div>
					
					
				    <div id="review1" class="tab-pane fade">
					<?php if(!$reviews){ ?>
						
						<h3>No reviews Yet!</h3>
						
					<?php	
					}else{ ?>
				      <h3>CUSTOMER REVIEWS</h3>
					  <?php foreach($reviews as $k=>$v){ ?>
					  <div class="customerreview">
				      <h4 class="custheadingreview"><?php echo $v['review']; ?></h4>

				      <p><span class="custreview">Quality :</span>&nbsp;
				      		<span class="fa fa-star checked"></span>
							<span class="fa fa-star <?php if($v['rating']>=2){ echo "checked";} ?>"></span>
							<span class="fa fa-star <?php if($v['rating']>=3){ echo "checked";} ?>"></span>
							<span class="fa fa-star <?php if($v['rating']>=4){ echo "checked";} ?>"></span>
							<span class="fa fa-star <?php if($v['rating']>=5){ echo "checked";} ?>"></span>
				      </p>
				      <p><span class="custreview">Review by :</span>&nbsp;
				      		<span><?php echo $v['customer_name']; ?></span></p>
				      <p><span class="custreview">Posted on :</span>&nbsp;
				      		<span><?php echo $v['posted_on']; ?></span></p>
						</div>
					  <?php } 
					  }?>
						
						
				    </div>
					
					
				</div>
			</div>
		</div>
	</div>
</section>

<section id="relatedproduct">
	<div class="container">
	
	<?php if(!$related){ ?>
		
		<div class="col-md-6 relatedtitle" style="margin-bottom:40px;">
		
				<h2>No Related Products Found</h2>
				
			</div>
			
			
			<?php 
			}else{ 
			
			?>
		<div class="row">
			<div class="col-md-6 relatedtitle">
				<h2>RELATED PRODUCTS</h2>
				
			</div>
			<div class="col-md-6 hidden-xs hidden-sm relatedbtn">
				<button id="prevrelated"><i class="fa fa-angle-left"></i></button>
			 	<button id="nextrelated"><i class="fa fa-angle-right"></i></button>
			</div>
		</div>
		<div class="row">
			<section class="relatedslider slider" id="relatedslider1">
					<?php 

						foreach($related as $key=>$value){
					?>
					<div class="slide2div">
						<div class="image-container">
						  <a href=""><img src="<?php echo base_url().$value['images']; ?>" alt="Avatar" class="image" style="width:100%"></a>
						  <div class="middle">
						    <div class="text">
						    	<p>
						 			<span><a href=""><i class="fa fa-plus"></i> ADD TO CART</a></span>
						 			<span><a href=""><i class="fa fa-eye"></i></a></span>
						 			<span><a href=""><i class="fa fa-heart"></i></a></span>
						 			<span><a href=""><i class="fa fa-exchange"></i></a></span>
						 		</p>
						    </div>
						  </div>
						</div>
					    <a href="" class="link"><h4><?php echo $value['product_name']; ?></h4></a>
					    <p><span class="fa fa-star checked"></span>
							<span class="fa fa-star checked"></span>
							<span class="fa fa-star checked"></span>
							<span class="fa fa-star"></span>
							<span class="fa fa-star"></span>
						</p>
						<p>
						<?php if($value['offer_price'] != ""){ ?>
						
					    <span class="price1"><i class="fa fa-rupee"></i><?php echo $value['offer_price'];?></span>&nbsp;
						
						<?php } ?>
						
					    	<span class="<?php echo($value['offer_price'] != "" ? 'price2' : 'price1'); ?>"><i class="fa fa-rupee"></i><?php echo $value['price'];?></span></p>
					    
				    </div>
					
			
						<?php }
						
					
					?>
						
						
						
					
			</section>
		</div>
		
					<?php } ?>
	</div>
</section>