<?PHP
class Productmodel extends CI_Model
{	
	function getcategory($cat){
		$this->db->select('category');
		$this->db->from('tbl_category');
		$this->db->where("cat_id",$cat);
		$query = $this->db->get(); 
		if($query->num_rows() >= 1){
			return $query->result_array();
		}else{
			return false;
		}
	}
	
	function getsubcategory($subcat){
		$this->db->select('subcategory');
		$this->db->from('tbl_subcategory');
		$this->db->where("subcat_id",$subcat);
		$query = $this->db->get(); 
		if($query->num_rows() >= 1){
			return $query->result_array();
		}else{
			return false;
		}
	}
	
	function getkeyIngredients($cat){
		$this->db->select('*');
		$this->db->from('tbl_keyingredients');
		$this->db->where("category_id",$cat);
		$this->db->where("is_active",1);
		$query = $this->db->get(); 
		if($query->num_rows() >= 1){
			return $query->result_array();
		}else{
			return false;
		}
	}
	
	function getconcernFilters($cat){
		$this->db->select('*');
		$this->db->from('tbl_concern');
		$this->db->where("category_id",$cat);
		$this->db->where("is_active",1);
		$query = $this->db->get(); 
		if($query->num_rows() >= 1){
			return $query->result_array();
		}else{
			return false;
		}
	}
	
	
}
	

?>
