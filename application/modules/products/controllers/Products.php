<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Products extends CI_Controller 
{
	
	function _remap($method_name = 'index')
	{ 
        if(!method_exists($this, $method_name))
		{ 
			$this->index(); 
        } 
        else
		{
			$this->{$method_name}(); 
        } 
    }
	
	function __construct(){
		parent::__construct();
		
		$this->load->model('productmodel');
		$this->load->model('home/homemodel');
	}
	
	function index() {
		$category = urlencode($this->uri->segment(4));//Main Category id
		$type = urlencode($this->uri->segment(2));//Type name 
		$act = str_replace("%26","&",str_replace("-"," " ,urlencode($this->uri->segment(3))));//Subcategory Id or Name
		
		if($type == "category"){
			
			$sql = "SELECT * FROM tbl_products WHERE category_id='".$category."' AND subcategory_id='".$act."' AND status=1";
			 $subcat = $this->productmodel->getsubcategory($act);
			 $data['subcategoryName'] = $subcat[0]['subcategory'];
			 $data['subcatId'] = $act;
		}
		if($type == "concern"){
			
			$sql = "SELECT * FROM tbl_products WHERE category_id='".$category."' AND concern LIKE '%".$act."%' AND status=1";
			$data['subcategoryName'] = $act;
			$data['subcatId'] = "";
			
		}
		if($type == "ethos"){
		
			$sql = "SELECT * FROM tbl_products WHERE category_id='".$category."' AND ethos LIKE '%".$act."%' AND status=1";
			$data['subcategoryName'] = $act;
			$data['subcatId'] = "";
			
		}
		/* For Listing Heading */
		$data['categoryName']= $this->productmodel->getcategory($category);
		$data['cat'] = $category;
		
		
		/* Category and Subcategory wise products */
		$query = $this->db->query($sql);
		$data['products'] = $query->result_array();
		
		/* Keyingredients For filters */
		$data['keyIngredients'] = $this->productmodel->getkeyIngredients($category);
		
		/* Concern For filters */
		$data['concernfilter'] = $this->productmodel->getconcernFilters($category);
		
		
		/*For Mega menu Bar*/
		$data['menu'] = $this->homemodel->getMenuData();
		$data['category'] = $this->homemodel->getsubMenuData();
		$data['concern'] = $this->homemodel->getconcerns();
		$data['ethos'] = $this->homemodel->getethos();
		
		/*For cart*/
		if(isset($_SESSION['cart_id'])){
			
			$cart_id = $this->session->cart_id;
			$data['cart'] = $this->homemodel->getCartdata($cart_id);
			
		}else{
			
			$data['cart'] = false;
			
		}
		
		$this->load->view('includes/header',$data);
		$this->load->view('products/products');
		$this->load->view('includes/footer');
		
	}
	
	function filter(){
		
		$concern = str_replace("&amp;", "&",$this->input->post('concrn'));
		$keying = $this->input->post('keyingd');
		$cat = $this->input->post('cat');
		$subcat = $this->input->post('subcatid');
		
		if($subcat != ""){ $sql_part = "AND subcategory_id=".$subcat." ";}else{ $sql_part = "";}
		
		
		$sql = "SELECT * FROM tbl_products WHERE status=1 ".$sql_part."AND category_id=".$cat." AND key_ingredients LIKE '%".$keying."%' AND concern LIKE '%".$concern."%'";
		$query = $this->db->query($sql);
		$data['products'] = $query->result_array();
		
		$this->load->view('products/filter',$data);
		
	}
	
	
	
}

?>
