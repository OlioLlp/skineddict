</header>

<section class="maincontent">
	<div class="container">
		<div class="row mainlisting">
			<div class="col-md-3">
				<div class="filter-option hidden-sm hidden-xs">
				<!-- CONCERN FILTER-->
				<?php if($concernfilter !== FALSE){?>
					<h3 class="filter-options-title">Concern</h3>
					<hr>
					<dl>
					<?php
					foreach($concernfilter as $kk=>$vv){
					?>
					  <dd>
					  	<a href="javascript:void(0)"><span class="listarrw"><i class="fa fa-angle-right"></i></span>
						<span onclick="changeconcern(this)"><?php echo $vv['concern']; ?></span>
						</a>
					  </dd>
					  <hr>
					 <?php
					}
					?> 
					  
					</dl>
				<?php }?>
				<!-- KEY INGREDIENTS FILTER-->
				<?php if($keyIngredients !== FALSE){?>
					<h3 class="filter-options-title">Key Ingredients</h3>
					<hr>
					<dl>
					<?php
					foreach($keyIngredients as $k=>$v){
					?>
					<dd>
					  	<a href="javascript:void(0)"><span class="listarrw"><i class="fa fa-angle-right"></i></span>
						<span onclick="changekeying(this)"><?php echo $v['ingredient']; ?></span>
						</a>
					  </dd>
					  <hr>
					<?php
					}
					?>
					  
					</dl>
					<?php }?>
					
					<!-- PRICE FILTER -->
					<h3 class="filter-options-title">PRICE</h3>
					<hr>
					<dl>
					  <dd>
					  	<a href="javascript:void(0)"><span class="listarrw"><i class="fa fa-angle-right"></i></span>
						<span>€0.00 - €99.99</span>
						<span class="listcount">(5)</span></a>
					  </dd>
					  <hr>
					  <dd>
					  	<a href="javascript:void(0)"><span class="listarrw"><i class="fa fa-angle-right"></i></span>
						<span>€100.00 and above</span>
						<span class="listcount">(14)</span></a>
					  </dd>
					</dl>
				</div>

				<div class="hidden-md hidden-lg"><button type="button" class="btn btn-reserve" data-toggle="collapse" data-target="#filterdemo1"><i class="fa fa-filter"></i>  FILTERS</button></div>
				<div class="hidden-md hidden-lg collapse" id="filterdemo1">
					<form>
						<!-- <h5 class="filter-title">FILTERS</h5> -->
						<?php if($concernfilter !== FALSE){?>
						<select class="selectpicker btnwidth" data-style="btn-primary btn-info" data-live-search="true" onchange="mobchangeconcern(this)" id="catlist">
							<option value="" selected="selected">Concern</option>
							<?php
							foreach($concernfilter as $kk=>$vv){
							?>
							<option><?php echo $vv['concern']; ?></option>
							
					<?php } ?>
							
						</select>
						<?php } ?>
						
						<?php if($keyIngredients !== FALSE){?>
						<select class="selectpicker btnwidth" data-style="btn-primary btn-info" data-live-search="true" onchange="mobchangekeying(this)" id="locality">
							<option value="" selected="selected">Key Ingredients</option>
							<?php
							foreach($keyIngredients as $k=>$v){
							?>
							
							<option><?php echo $v['ingredient']; ?></option>
							<?php } ?>
							
						</select>
						<?php } ?>
						
						<select class="selectpicker btnwidth" data-style="btn-primary btn-info" data-live-search="true" id="classfor2">
							<option value="" selected="selected">Price</option>
							<option>€0.00 - €99.99</option>
							<option>€100.00 and above</option>
						</select>
					</form>
				</div>
			</div>
			<div class="col-md-9">
				<div class="category-image">
					<img src="<?php echo base_url(); ?>assets/images/category-image1.jpg" class="img-responsive">
				</div>
				<h1 class="category-title"><?php echo $categoryName[0]['category']; ?></h1>
				<div class="row">
					<div class="col-md-6">
						<div class="countproduct">
							<span class="bold"><?php echo $subcategoryName; ?>:</span>&nbsp;&nbsp;
							<span><?php echo count($products); ?> PRODUCTS</span>
						</div>
						<!-- <div id="btnlistgrid">
							<button class="btn-view" onclick="listView()"><i class="fa fa-bars"></i> </button> 
							<button class="btn-view active" onclick="gridView()"><i class="fa fa-th-large"></i></button>
						</div> -->
					</div>
					<div class="col-md-6">
						<div class="sortby">
							<label>sort by</label>
							<select>
								<option>Price</option>
								<option>Position</option>
								<option>Produce by</option>
							</select>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
				</div>
				<hr>
				<div class="row" id="productList">
				<?php foreach($products as $key=>$value){ ?>
					<div class="col-md-4 col-sm-12 col-xs-12">
						<div class="rowlist">
							<div class="image-container">
							  <a href="<?php echo base_url(); ?>details/<?php echo $value['product_id']; ?>"><img src="<?php echo base_url().$value['images'];?>" alt="Avatar" class="img-responsive" style="width:100%;"></a>
							  <div class="middle">
							    <div class="text">
							    	<p>
							 			<span><a href=""><i class="fa fa-plus"></i> ADD TO CART</a></span>
							 			<span><a href=""><i class="fa fa-eye"></i></a></span>
							 			<span><a href=""><i class="fa fa-heart"></i></a></span>
							 			<span><a href=""><i class="fa fa-exchange"></i></a></span>
							 		</p>
							    </div>
							  </div>
							</div>
						    <a href="<?php echo base_url(); ?>details/<?php echo $value['product_id']; ?>" class="link"><h4><h4><?php echo $value['product_name'];?></h4></a>
						    
						   <?php if($value['offer_price'] != ""){ ?>
						
					    <p><span class="price1"><i class="fa fa-rupee"></i><?php echo $value['offer_price'];?></span>&nbsp;
						
						<?php } ?>
						
					    	<span class="<?php echo($value['offer_price'] != "" ? 'price2' : 'price1'); ?>"><i class="fa fa-rupee"></i><?php echo $value['price'];?></span></p>
						</div>
					</div>
					
					<?php } ?>
					  
				</div>
			</div>
		</div>
	</div>
</section>

<script>
concern = "";
keyingredient = "";
category = "<?php echo $cat; ?>";
subcatID = "<?php echo $subcatId; ?>";
function filter(){
	
	$.ajax({
			url: "<?php echo base_url(); ?>products/filter",
			method:"POST",
	        data:{
				concrn : concern,
				keyingd : keyingredient,
				cat : category,
				subcatid : subcatID
				
			}, 
            success: function(data){
				$('#productList').html('').append(data);
				
			}
			});
	
	
	
}

function changeconcern(ele){
	
	concern = ele.innerHTML;
	filter();
	
}

function changekeying(element){
	
	keyingredient = element.innerHTML;
	filter();
		
}

function mobchangeconcern(mele){
	
	
	concern = mele.value;
	filter();
	//console.log(concern);
}

function mobchangekeying(melement){
	
	keyingredient = melement.value;
	//console.log(keyingredient);
	filter();
}


</script>

