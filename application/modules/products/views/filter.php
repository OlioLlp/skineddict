<?php

if(count($products) == 0){ ?>
	
	<h4>0 Products Found</h4>
	
<?php	
}else{

 foreach($products as $key=>$value){ ?>
					<div class="col-md-4 col-sm-12 col-xs-12">
						<div class="rowlist">
							<div class="image-container">
							  <a href="<?php echo base_url(); ?>details/<?php echo $value['product_id']; ?>"><img src="<?php echo base_url().$value['images'];?>" alt="Avatar" class="img-responsive" style="width:100%;"></a>
							  <div class="middle">
							    <div class="text">
							    	<p>
							 			<span><a href=""><i class="fa fa-plus"></i> ADD TO CART</a></span>
							 			<span><a href=""><i class="fa fa-eye"></i></a></span>
							 			<span><a href=""><i class="fa fa-heart"></i></a></span>
							 			<span><a href=""><i class="fa fa-exchange"></i></a></span>
							 		</p>
							    </div>
							  </div>
							</div>
						    <a href="<?php echo base_url(); ?>details/<?php echo $value['product_id']; ?>" class="link"><h4><h4><?php echo $value['product_name'];?></h4></a>
						    
						   <?php if($value['offer_price'] != ""){ ?>
						
					    <p><span class="price1"><i class="fa fa-rupee"></i><?php echo $value['offer_price'];?></span>&nbsp;
						
						<?php } ?>
						
					    	<span class="<?php echo($value['offer_price'] != "" ? 'price2' : 'price1'); ?>"><i class="fa fa-rupee"></i><?php echo $value['price'];?></span></p>
						</div>
					</div>
					
					<?php } 
					
}
?>