
</header>

<section class="maincontent">
	<div class="container">
		<div class="row">
			<div class="cart-title">
				<h2>Shopping Cart</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-md-9">
				<div class="table-wrapper">
					<table>
						<thead>
							<tr style="border-top: 0px;">
								<th>ITEM</th>
								<th>Price</th>
								<th>QTY</th>
								<th>Subtotal</th>
							</tr>
						</thead>
						
						<tbody class="carttable">
							<tr>
								<td class="itemdetailcart">
									<img src="images/4.jpg" class="cartimg">
									<div class="product-title-cart">
										<a href=""><span class="cart-title1">AFTERSHAVE LOTION1</span></a>
									</div>
								</td>
								<td>€130.00</td>
								<td>1</td>
								<td>€130.00</td>
							</tr>
							<tr class="item-action">
								<td colspan="4">
									<i class="fa fa-edit"></i>&nbsp;<a href="">Edit</a>&nbsp;&nbsp;&nbsp;
									<i class="fa fa-trash"></i>&nbsp;<a href="">Remove item</a>
								</td>
							</tr>
							<tr>
								<td class="itemdetailcart">
									<img src="images/3_2 (1).jpg" class="cartimg">
									<div class="product-title-cart">
										<a href=""><span class="cart-title1">AFTERSHAVE LOTION1</span></a>
									</div>
								</td>
								<td>€150.00</td>
								<td>1</td>
								<td>€150.00</td>
							</tr>
							<tr class="item-action">
								<td colspan="4">
									<i class="fa fa-edit"></i>&nbsp;<a href="">Edit</a>&nbsp;&nbsp;&nbsp;
									<i class="fa fa-trash"></i>&nbsp;<a href="">Remove item</a>
								</td>
							</tr>
						</tbody>
					</table>
				</div>	
			</div>
			<div class="col-md-3">
				<div class="summary">
					<h2 class="summary-title">Summary</h2>
					<hr>
					<p style="padding: 0px 0px 20px;">
						<span class="leftsummary">Subtotal</span>
						<span class="rightsummary">€425.00</span>
					</p>
					<p>
						<span class="leftsummary bold">Shipping</span><span> (Flat Rate - Fixed)</span>
						<span class="rightsummary">€15.00</span>
					</p>
					<hr>
					<p>
						<span class="leftsummary">Order Total</span>
						<span class="rightsummary bold">€440.00</span>
					</p><br>
					<hr>
					<p id="open-discount">Apply Discount Code <i class="fa fa-angle-up pull-right"></i></p>
					<br>
					<div id="discount-code" class="animated slideIn">
						<label>Enter discount code</label>
						<input type="text" name="discount" class="form-control">
						<br>
						<button class="btn btn-signbtn">apply discount</button>
					</div>
					<br>
					<a href="checkout.php"><button class="btn btn-signbtn">Proceed to checkout</button></a>
				</div>
			</div>
		</div>
		<div class="row cartbtn1">
			<a href="index.php" ><button class="btn btn-signbtn cartbtn">continue shopping</button></a>&nbsp;&nbsp;
			<button class="btn btn-signbtn cartbtn">clear shopping cart</button>&nbsp;&nbsp;
			<button class="btn btn-signbtn cartbtn">upadate shopping cart</button>
		</div>
	</div>
</section>

<script type="text/javascript">
$('#open-discount').on("click", function(){
	 $("#discount-code").slideToggle(500);
});
</script>
