<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class cart extends CI_Controller 
{
	
	function __construct(){
		parent::__construct();
		
		$this->load->model('home/homemodel');
		
		
	}
	
	function index() {
		
		$data['menu'] = $this->homemodel->getMenuData();
		$data['category'] = $this->homemodel->getsubMenuData();
		$data['concern'] = $this->homemodel->getconcerns();
		$data['ethos'] = $this->homemodel->getethos();
		
		
		if(isset($_SESSION['cart_id'])){
			
			$cart_id = $this->session->cart_id;
			$data['cart'] = $this->homemodel->getCartdata($cart_id);
			
		}else{
			
			$data['cart'] = false;
			
		}
		
		$this->load->view('includes/header',$data);
		$this->load->view('cart/cart');
		$this->load->view('includes/footer');
		
	}
	
	
}

?>
