<?PHP
class Cartmodel extends CI_Model
{	
	function getMenuData(){
		$this->db->select('*');
		$this->db->from('tbl_category');
		$this->db->where("is_delet",0);
		$query = $this->db->get(); 
		if($query->num_rows() >= 1){
			return $query->result_array();
		}else{
			return false;
		}
	}
	
	function getsubMenuData(){
		$this->db->select('*');
		$this->db->from('tbl_subcategory');
		$this->db->where("is_delet",0);
		$query = $this->db->get(); 
		if($query->num_rows() >= 1){
			return $query->result_array();
		}else{
			return false;
		}
	}
	
	function getconcerns(){
		$this->db->select('*');
		$this->db->from('tbl_concern');
		$this->db->where("is_active",1);
		$query = $this->db->get(); 
		if($query->num_rows() >= 1){
			return $query->result_array();
		}else{
			return false;
		}
	}
	
	function getethos(){
		$this->db->select('*');
		$this->db->from('tbl_ethos');
		$this->db->where("is_active",1);
		$query = $this->db->get(); 
		if($query->num_rows() >= 1){
			return $query->result_array();
		}else{
			return false;
		}
	}
	
	function mainslider(){
		$this->db->select('*');
		$this->db->from('tbl_homeslider');
		$this->db->where("status",1);
		$this->db->order_by("order_by", "asc");
		$query = $this->db->get(); 
		if($query->num_rows() >= 1){
			return $query->result_array();
		}else{
			return false;
		}
		
		
	}
	
	
	function getFeaturedProd(){
		$this->db->select('*');
		$this->db->from('tbl_products');
		$this->db->where("status",1);
		$this->db->where("is_featured",1);
		$query = $this->db->get(); 
		if($query->num_rows() >= 1){
			return $query->result_array();
		}else{
			return false;
		}
		
		
	}
	
	function getBestSellerProd(){
		$this->db->select('*');
		$this->db->from('tbl_products');
		$this->db->where("status",1);
		$this->db->where("is_best_seller",1);
		$query = $this->db->get(); 
		if($query->num_rows() >= 1){
			return $query->result_array();
		}else{
			return false;
		}
		
		
	}
	
	function getTrendingProd(){
		$this->db->select('*');
		$this->db->from('tbl_products');
		$this->db->where("status",1);
		$this->db->where("is_trending",1);
		$query = $this->db->get(); 
		if($query->num_rows() >= 1){
			return $query->result_array();
		}else{
			return false;
		}
		
		
	}
	
	function getHotdeals(){
		$this->db->select('*');
		$this->db->from('tbl_hotdeals');
		$this->db->where("status",1);
		$query = $this->db->get(); 
		if($query->num_rows() >= 1){
			return $query->result_array();
		}else{
			return false;
		}
		
		
	}
	
	
	function getAboutusManagement(){
		$this->db->select('*');
		$this->db->from('tbl_aboutus_management'); 
		$this->db->where("status","Active");
		$query = $this->db->get(); 
		if($query->num_rows() >= 1){
			return $query->result();
		}else{
			return false;
		}
	}
	function getAboutusCreative(){
		$this->db->select('*');
		$this->db->from('tbl_aboutus_creativeteam'); 
		$this->db->where("status","Active");
		$query = $this->db->get(); 
		if($query->num_rows() >= 1){
			return $query->result();
		}else{
			return false;
		}
	}
	
	function getCartdata($cart){
		
		$this->db->select(array('tbl_cart.*','tbl_products.*'));
		$this->db->from('tbl_cart');
		$this->db->join('tbl_products', 'tbl_products.product_id = tbl_cart.product_id');
		$this->db->where("tbl_cart.customer_cart_id",$cart);
		$this->db->where("tbl_cart.cart_type",1);
		$this->db->where("tbl_cart.is_deleted",0);
		$query = $this->db->get(); 
		if($query->num_rows() >= 1){
			return $query->result_array();
		}else{
			return false;
		}
	
	}
	
	function checkToCart($prd_id,$qty){
		
		
		$this->db->select('cart_id');
		$this->db->from('tbl_cart');
		$this->db->where('customer_cart_id',$_SESSION['cart_id']);
		$this->db->where('product_id',$prd_id);
		$query = $this->db->get();
		if($query->num_rows() >= 1){
			return true;
		}else{
			return false;
		}
		
	}
	
	function addTocart($prd_id,$qty){
		
		$data = array(
        'customer_cart_id' => $_SESSION['cart_id'],
        'product_id' => $prd_id,
        'quantity' => $qty,
		'cart_type' => 1,
		'is_deleted' => 0
		);

	 $this->db->insert('tbl_cart', $data);
		
		
		
	}
	
	function updateCart($prd_id,$qty){
		
		$this->db->set('quantity', 'quantity+'.$qty, FALSE);
		$this->db->where('customer_cart_id', $_SESSION['cart_id']);
		$this->db->where('product_id', $prd_id);
		$this->db->update('tbl_cart');
		
		
	}
	
	
}
	

?>
